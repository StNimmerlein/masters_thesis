\select@language {english}
\contentsline {chapter}{Abstract}{1}{section*.1}
\contentsline {chapter}{\numberline {0}Introduction}{2}{chapter.0}
\contentsline {chapter}{\numberline {1}Low Discrepancy Sequences}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Descrepancy}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}$(t, s)$-Sequences}{5}{section.1.2}
\contentsline {chapter}{\numberline {2}Fundamentals of Function Theory}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Valuation Rings}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Function Fields, Places and Divisors}{9}{section.2.2}
\contentsline {chapter}{\numberline {3}Implementing}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Discrepancy}{13}{section.3.1}
\contentsline {chapter}{\numberline {4}Conclusion}{17}{chapter.4}
\contentsline {chapter}{Appendix \numberline {A}Some extra stuff}{18}{Appendix.1.A}
\contentsline {chapter}{References}{19}{chapter*.5}
