%!TEX root = ../dissertation.tex
%\begin{savequote}[75mm]
%Nulla facilisi. In vel sem. Morbi id urna in diam dignissim feugiat. Proin molestie tortor eu velit. Aliquam erat %volutpat. Nullam ultrices, diam tempus vulputate egestas, eros pede varius leo.
%\qauthor{Quoteauthor Lastname}
%\end{savequote}

\chapter{Low Discrepancy Sequences} \label{ch:lds}

First we have to explain how the discrepancy of a point set or sequence is defined. The definitions and most statements are taken from \cite{Niederreiter1} and \cite{Niederreiter2}. We also prove a lemma that could be used to calculate the exact discrepancy, although this process is too expensive for a large set of points (see Chapter \ref{ch:implementation} for another approach). Finally we define $(t, s)$-sequences which we will create later in Chapter \ref{ch:construction} and for which we can give an upper bound for the discrepancy.

\section{Discrepancy}

\begin{definition}\label{def:discrepancy}\index{Discrepancy}
	Let $X = \{x_1, \ldots, x_n\} \subset I^s$ be a set of $n$ points in the $s$-dimensional half-open unit cube $I^s = [0, 1)^s, s \geq 1$, and $J \subseteq I^s$ a subinterval. With $A(J; n)$ the number of points $x_i \in X \cap J$ of the set that lie in $J$ and $V(J)$ the volume of $J$, we put
	$$ D(J; n) \coloneqq A(J; n) - V(J) \cdot n.$$ 
	Now we can define the \emph{discrepancy} $\Delta(n)$ of the point set to be
	$$ \Delta(n) \coloneqq sup_{J = \prod_{i=1}^s [0, j_i) \subseteq I^s} |D(J; n)|.$$
	For a point sequence $x_1, x_2, \ldots$ we define $\Delta(n)$ to be the discrepancy of the first $n$ points.
\end{definition}
\begin{remark}
	As you can see, the discrepancy $\Delta(n)$ of $n$ points gives some kind of measurement for the uniformity of the distribution of those points since it is the maximal difference between the number of points lying in an interval and the number of points we would expect to lie in it, based on its volume.
\end{remark}
\begin{lemma}\label{exactdiscrepancy}
	For a set of $n$ points $X = \{x_1, \ldots, x_n\} \subset I^s$ there exists at least one of the following:
	\begin{enumerate}
		\item a half-open interval $O = \prod_{i=1}^s [0,o_i) \subseteq I^s$ such that $|D(O; n)| = \Delta(n)$
		\item a closed interval $C = \prod_{i=1}^s [0, c_i] \subseteq \bar{I^s}$ such that $|D(C; n)| = \Delta(n)$
	\end{enumerate}
\end{lemma}
\begin{proof}
	If $\Delta(n) = 0$ take $O = I^s$. So now $\Delta(n) \neq 0$. 
	Let $Y \subseteq X$ be a subset of the points in $X$.
	For every dimension $1 \leq i \leq s$ we denote by $c_i \coloneqq \max(\proj_i(Y) \cup \{0\})$ the maximal coordinate $\proj_i(y)$ in dimension $i$ of the points in $Y$ and by $o_i \coloneqq \min(\proj_i(X \setminus Y) \cup \{1\})$ the minimal one of the points outside of $Y$.
	Finally we define $O_Y \coloneqq \prod_{i=1}^s [0, o_i) \subseteq I^s$ and $C_Y \coloneqq \prod_{i=1}^s [0, c_i] \subseteq \bar{I^s}$.
	We claim that
	$$\Delta(n) = \max_{Y \subseteq X} \{|D(O_Y; n)|, |D(C_Y; n)|\}.$$
	To prove this we show that for a fixed $Y \subseteq X$ and every $J = \prod_{i=1}^s [0, j_i) \subseteq I^s$ such that $J \cap X = Y$ we have $|D(J; n)| \leq \max\{|D(O_Y; n)|, |D(C_Y; n)|\}$. Then we have essentially for any $J' \subseteq I^s$ that $|D(J'; n)| \leq \max_{Y \subseteq X} \{|D(O_Y; n)|, |D(C_Y; n)|\}$ and we are done. For $J$ we have two cases:
	\begin{enumerate}
		\item $A(J; n) \geq V(J) \cdot n$: Since $X \cap C_Y = Y = X \cap J$, we have $A(C_Y; n) = A(J; n)$. However, $c_i \leq j_i$ by definition and therefore $V(C_Y) \leq V(J)$. So we get
		\begin{align*}
		|D(C_Y; n)| &= |A(C_Y; n) - V(C_Y) \cdot n| \\
				    &\geq A(C_Y; n) - V(C_Y) \cdot n\\
				    &\geq A(J; n) - V(J) \cdot n \\
				    &= |A(J; n) - V(J) \cdot n|\\
				    & = |D(J; n)|.
		\end{align*}
		\item $A(J; n) < V(J) \cdot n$: Again we have $A(O_Y; n) = A(J; n)$. Now $o_i \geq j_i$ thus $V(O_Y) \geq V(J)$ and finally
		$$|D(O_Y; n)| \geq |D(J; n)|.$$
	\end{enumerate}
\end{proof}

\begin{figure}
	\centering
	\begin{minipage}[t]{.5\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/uniform16Pts}
		\captionof{figure}{This uniformly distributed point set in $I^2$ has discrepancy $3.75$.}
		\label{fig:uniform16Pts}
	\end{minipage}%
	\begin{minipage}[t]{.5\textwidth}
		\centering
		\includegraphics[width=\linewidth]{figures/halfuniform16Pts}
		\captionof{figure}{The points of this set lie only in the lower half of the unit square, hence the higher discrepancy of $9.875$.}
		\label{fig:halfuniform16Pts}
	\end{minipage}
\end{figure}
	
\begin{example}
	\leavevmode
	\begin{enumerate}
		\item The point set $\{(x, y) \mid x, y \in \{\frac{1}{8}, \frac{3}{8}, \frac{5}{8}, \frac{7}{8}\}\}$ consisting of $16$ points is uniformly distributed in the unit square (see Figure \ref{fig:uniform16Pts}). It's discriminant $\Delta(16)$ is $3.75$.
		\item The points of the set $\{(x, y) \mid x, 2y \in \{\frac{1}{8}, \frac{3}{8}, \frac{5}{8}, \frac{7}{8}\}\}$ only lie in the lower half of the unit square (see Figure \ref{fig:halfuniform16Pts}). Here the discriminant $\Delta(16)$ is $9.875$.
	\end{enumerate}
\end{example}

The discrepancy $\Delta(n)$ of a point set or sequence increases with $n$, so to get low-discrepancy point sets and sequences we would like to find for any $n \geq 2$, points in $I^s$ with
	$$ \Delta(n) \leq B_s(\log n)^{s-1} + O((\log n)^{s-2})$$
and sequences of points in $I^s$ with
	$$ \Delta(n) \leq C_s(\log n)^s + O((\log n)^{s-1}) $$
for all $n \geq 2$, where the constants $B_s$ and $C_s$ are as small as possible.\\
In this thesis we are mostly interested in finite point sets that can be used for numerical methods, but low-discrepancy sequences obviously give raise to low-discrepancy point sets, so sequences with good $C_s$ should give sets with good $B_s$.

\section{$(t, s)$-Sequences}
$(t,s)$-sequences are one type of sequences for which we can compute an effective bound, that is, we can give an explicit value for $C_s$. To define them we first need to introduce $(t,m,s)$-nets and elementary intervals. Let the dimension $s \geq 1$ and an integer $b \geq 2$ in this section be fixed. 
\begin{definition}\index{Elementary interval}
	An interval of the form
	$$ J = \prod_{i=1}^{s} [a_i b^{-d_i}, (a_i+1) b^{-d_i}) $$
	with integers $d_i \geq 0$, $0 \leq a_i < b^{d_i}$ for $1 \leq i \leq s$ is called an \emph{elementary interval in base $b$}.
\end{definition}

\begin{definition}\index{$(t, m, s)$-net}
	Let $0 \leq t \leq m$ be integers. A \emph{$(t,m,s)$-net in base $b$} is a point set of $b^m$ points in $I^s$ such that $A(J; b^m) = b^t$ for every elementary interval $J$ in base $b$ with $V(J) = b^{t-m}$.
\end{definition}

\begin{definition}\index{$(t, s)$-sequence}
	Let $t \geq 0$ be an integer. A sequence $x_1, x_2, \ldots$ of points in $I^s$ is called a \emph{$(t,s)$-sequence in base $b$} if for all integers $k \geq 0$ and $m \ge t$ the point set consisting of the $x_n$ with $kb^m < n \leq (k+1)b^m$ is a $(t,m,s)$-net in base $b$.
\end{definition}

In \cite{Niederreiter1} it is shown that the discrepancy of a $(t,s)$-sequence in base $b$ satisfies the effective bound
$$ \Delta(n) \leq C(t,s,b)(\log n)^s + O((\log n)^{s-1}) $$
for all $n \geq 2$, where $C(t,s,b)$ is given as
\begin{align*}
	& C(t,s,b)=\frac{1}{8}b^t(\frac{b-1}{\log b})^2	& & \text{~for $s = 2$,}\\
	& C(t,s,b)=\frac{2^t}{24(\log 2)^3}				& & \text{~for $s = 3$ and $b = 2$,}\\
	& C(t,s,b)=\frac{2^t}{64(\log 2)^4}				& & \text{~for $s = 4$ and $b = 2$,}\\
	& C(t,s,b)=\frac{1}{s!}b^t\frac{b-1}{2\lfloor b/2 \rfloor}(\frac{\lfloor b/2 \rfloor}{\log b})^s & & \text{~in all other cases.}
\end{align*}

In this thesis we will focus on such $(t,s)$-sequences, especially those that were constructed by means of function fields. So before we get to the explicit construction in Chapter \ref{ch:construction} we first introduce the main aspects of function field theory in the next chapter.