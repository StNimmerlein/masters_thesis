%!TEX root = ../dissertation.tex
%\begin{savequote}[75mm]
%Nulla facilisi. In vel sem. Morbi id urna in diam dignissim feugiat. Proin molestie tortor eu velit. Aliquam erat volutpat. Nullam ultrices, diam tempus vulputate egestas, eros pede varius leo.
%\qauthor{Quoteauthor Lastname}
%\end{savequote}

\chapter{Implementation}\label{ch:implementation}
% Listing style
\lstset{language=Magma, basicstyle=\footnotesize\ttfamily} % numbers=left, numbersep=2pt, numberstyle=\tiny\color{mygrey}

This chapter will examine how the construction in Chapter \ref{ch:construction} can be implemented as an algorithm. We will give examples of implementations in the computer algebra system \textit{Magma} \cite{Magma} developed by the Computational Algebra Group within the School of Mathematics and Statistics at the University of Sydney.\\
First we will show and explain code that is easy to understand and after this give an implementation that does exactly the same but utilizes some of Magma's special features, such as formal constructors. Usually those implementations are not as easy to understand at first glance but are much more mathematical than the step-by-step construction via loops. Note that a ``\verb+\+'' at the end of a line indicates that there should be no line break.\\
For the whole chapter let $q$ be a fixed prime power, $s \leq 1$ an integer and $K/\mathbb{F}_q$ a function field over $\mathbb{F}_q$ with at least $s+1$ rational places $P_\infty, P_1, \ldots, P_s$.
As we will see in the first section, depending on the number $n_{\text{max}}$ of points we want to construct, we choose $r_{\text{max}} \geq \min \{i \mid q^i > n_{\text{max}}\}$. We could also dynamically increase $r_\text{max}$ but then we have to make sure that we have $\psi_r(0) = 0$ for all $\psi_r$ with $r$ larger than our initial $r_\text{max}$. However, a choice of $r_\text{max}$ high enough should suffice in most cases so we will not change it. We also choose a $j_{\text{max}} \geq 1$ that will determine the accuracy of our points in a sense that all points will lie on a square lattice with lattice width $q^{-j_{\text{max}}}$.

\section{Preliminary Considerations}\label{sec:prelim}

In (\ref{eq:consy}) and (\ref{eq:consx}) we have infinite sums so at first we are interested whether for fixed $n$ only finitely many summands are non-zero or if we at least could only compute the sums for finitely many summands without damaging the results too much. Luckily, this can be answered positively.
\begin{lemma}\label{lem:cjri}
	Only finitely many summands of the sum $\sum_{r=0}^\infty c_{j, r}^{(i)} \psi_r (a_r(n))$ in (\ref{eq:consy}) are non-zero.
\end{lemma}
\begin{proof}
	The $a_r(n)$ were defined to be the $r$-th digit in the digit expansion of $n$ in base $b$, thus for $r \geq \min \{i \mid b^i > n\}$ we have $a_r(n) = 0$. We also know that for sufficiently large $r$, say $r \geq l$ for a large integer $l$, we have $\psi_r(0) = 0$. So for $r \geq \max \{\min \{i \mid b^i > n\}, l\}$ we have $c_{j, r}^{(i)} \psi_r (a_r(n)) = c_{j, r}^{(i)} \cdot 0 = 0$ and the sum is well defined.
\end{proof}
The same does not hold for (\ref{eq:consx}). However, it is easy to see that we will get acceptable results if we just compute the sum for the first $j_{\text{max}}$ summands with $j_{\text{max}}$ sufficiently large: Each $x_n^{(i)}$ is a real number in the interval $[0, 1]$, given as a base $b$ expansion where the $j$-th summand gives the $b^{-j}$'s digit. So the difference between the complete sum $\sum_{j=1}^\infty y_{n, j}^{(i)} b^{-j}$ and the partial sum $\sum_{j=1}^{j_{\text{max}}} y_{n, j}^{(i)} b^{-j}$ is at most
$$|\sum_{j=1}^\infty y_{n, j}^{(i)} b^{-j} - \sum_{j=1}^\infty y_{n, j}^{(i)} b^{-j}| \leq b^{-j_{\text{max}}}.$$
Depending on the data type that we use for our numbers (eg. \textit{double}) we can choose $j_{\text{max}}$ large enough to gain the maximal accuracy this data type allows. Even for infinitely accurate abstract data types a choice of a large enough $j_{\text{max}}$ is sufficient for numerical calculations.\\
Note that the consideration above only holds for a finite number of points to be generated whereas for an infinite sequence the error is not as much negligible. However, for numerical calculations we only need a finite number of points so we are satisfied with this.

\section{Finding Bijections}\label{sec:findingbij}

We need to find bijections $\psi_r : B \rightarrow \mathbb{F}_q$ and $\eta_j^{(i)} : \mathbb{F}_q \rightarrow B$ for $B = \{0, \ldots, q-1\} = \mathbb{Z}\sslash q\mathbb{Z}$, $0 \leq r \leq r_\text{max}$, $1 \leq i \leq s$ and $1 \leq j \leq j_\text{max}$. Defining all those $sj_\text{max}+r_\text{max}+1$ bijections by hand is possible but not quite convenient. For our purposes (pseudo) randomly chosen bijections will do just fine so we need a method that, given two sets, returns a random bijection between these.\\
In Magma this can be done by forming two lists; one of all elements of the domain and one of all elements of the codomain. Then a random permutation is applied to the second list and finally a map is constructed that maps elements from the domain to elements in the codomain with the same index in the list.
\begin{lstlisting}[frame=single]
intrinsic GetRandomBijection(domain::Any, codomain::Any) -> Map
	{Returns a random bijection from domain to codomain.}
	domainSet := Set(domain);
	domainList := SetToSequence(domainSet);
	codomainSet := Set(codomain);
	codomainList := SetToSequence(codomainSet)^Random(Sym(codomainSet));
	graph := {domainList[i] -> codomainList[i] : i in Keys(domainList)};
	return map<domain -> codomain | graph>;
end intrinsic;
\end{lstlisting}
Magma will not recognize this map as a bijections but since we do not need its inverse it does not bother us.

\section{Computing the $c_{j, r}^{(i)}$}

The most complex part is computing the elements $c_{j, r}^{(i)}$ in (S\ref{it:cjri}). Luckily, Magma already provides most functions we need, especially the interaction with function fields and divisors. So let us assume that we are given a function field $K/\mathbb{F}_q$, a divisor $D$ with $\deg D = g(K)$ and $l(D) = 1$ and a list of rational places $P_\infty, P_1, \ldots, P_s$ of $K$ (in Magma stored as \texttt{PInf} and a list \texttt{P} with $\texttt{P[i]} = P_i$). As shown in the last section of Chapter \ref{ch:construction} we have to compute elements $k_j^{(i)}$ and $b_{j, r}^{(i)}$ to finally get the $c_{j, r}^{(i)}$ for each $1 \leq i \leq s$, $0 \leq r \leq r_\text{max}$, $1 \leq j \leq j_\text{max}$.\\
So now let $i$ and $j$ be fixed. To find an element $k_j^{(i)} \in K$ with $\ord_{P_i} (k_j^{(i)}) = -\ord_{P_i} (D) - j$ we look at the proof of Lemma \ref{lem:findk} and see that we can choose any element from $\mathcal{L}(D + jP_i) \setminus \mathcal{L}(D + (j - 1)P_i) \neq \emptyset$. So in Magma we create the Riemann-Roch spaces $\mathcal{L}(D + P_i)$ and $\mathcal{L}(D + (j - 1)P_i)$ together with the canonical map from those spaces to $K$.
%frames=single/leftline/lines/
\begin{lstlisting}[frame=single]
R, mR := RiemannRochSpace(D+(j)*P[i]);
S, mS := RiemannRochSpace(D+(j-1)*P[i]);
// mR maps from R to K, mS from S to K
\end{lstlisting}
To obtain our $k_j^{(i)}$ we then look at the quotient $\mathcal{L}(D + P_i) / \mathcal{L}(D + (j - 1)P_i)$ (which is non-trivial of dimension $1$ according to Lemma \ref{lem:dimension}). We choose a representative of the basis element (which then lies in $\mathcal{L}(D + P_i)$) and map it to $K$ to receive our element $k_j^{(i)}$.
\begin{lstlisting}[frame=single]
q, mq := quo<R|S>;
// q is the quotient with basis {q.1}, mq is the projection from R to q
kji := q.1 @@ mq @ mR;
\end{lstlisting}
Now we have to compute the elements $b_{j, r}^{(i)}$, $0 \leq r \leq r_\text{max} + 1$, from $k_j^{(i)}$. Note that to get $r_\text{max} + 1$ elements $c_{j, r}^{(i)}$ we have to compute $r_\text{max} + 2$ elements $b_{j, r}^{(i)}$. We can find the expansion in (\ref{eq:kji}) using Magma's \texttt{Expand} function. The precision $r_\text{max} + 1$ can be given as an optional argument and a local uniformizer $z$ will be found automatically. We store the coefficients of this expansion to convert them to a format we can work with more easily.
\begin{lstlisting}[frame=single]
// Get expansion in z, highest power of z in the expansion should be rmax+1
expansion, z := Expand(kji, PInf : AbsPrec := rmax+1);
coefficients, val, _ := Coefficients(expansion);
\end{lstlisting}
\texttt{coefficients} is a list of the coefficients starting at the first non-zero coefficient. The corresponding power to this is \texttt{val}, so the expansion begins with the term \verb|coefficients[1]*(z^val)|. We want to modify the list \texttt{coefficients} to start at the coefficient for $z^{-v}$ (recall $v = \ord_{P_\infty} D$) and extend it up to the coefficient for $z^{r_\text{max}-v}$. Therefore we create a new list and fill it with zeros or the matching entries from \texttt{coefficients}.
\begin{lstlisting}[frame=single]
bji := [];
// There are val+v zero coefficients before the first non-zero coefficient
for k in [1..(val+v)] do
	// We have to specify that the elements in b lie in F_q
	bji[k] := Fq!0;
end for;
// Then fill with coefficients
for k in Keys(coefficients) do
	bji[val+v+k] := coefficients[k];
end for;
// Fill the rest of b with 0 until it has length rmax+2
for k in [(#bji+1)..(rmax+2)] do
	bji[k] := Fq!0;
end for;
\end{lstlisting}
This can also be done nicer in one single line.
\begin{lstlisting}[frame=single]
bji := [Fq!0 : i in [1..(val+v)]] cat coefficients cat [Fq!0 : \
i in [1..(rmax+2-#coefficients-val-v)]];
\end{lstlisting}
Finally we get the $c_{j, r}^{(i)}$ as the $b_{j, r}^{(i)}$ without $b_{j, v}^{(i)}$. Note that $b_{j, r}^{(i)}$ is stored in \texttt{bji[r+1]}.
\begin{lstlisting}[frame=single]
cji := [];
for r in [1..Min(v, #bji)] do
	cji[r] := bji[r];
end for;
for r in [(Min(v, #bji)+1)..(rmax+1)] do
	cji[r] := bji[r+1];
end for;
\end{lstlisting}
Or, in one line:
\begin{lstlisting}[frame=single]
cji := [bji[r] : r in [1..Min(v, #bji)]] cat [bji[r+1] : \
r in [(Min(v, #bji)+1)..(rmax+1)]];
\end{lstlisting}
The parts above can be merged to the following Magma intrinsic.
\begin{lstlisting}[frame=single]
intrinsic FindCElements(K::FldFun, D::DivFunElt, s::RngIntElt, \
jmax::RngIntElt, rmax::RngIntElt) -> SeqEnum
	{Finds c[i][j][r+1] in (S4).}
	Fq := ConstantField(K);
	// Store all finite places of K of deg 1 in P ...
	P := [ p : p in Places(K, 1) | IsFinite(p)];
	// ... and an infinite rational place in PInf
	_ := exists(PInf){ p : p in InfinitePlaces(K) | Degree(p) eq 1 };

	v := Valuation(D, PInf);
	c := [];
	for i in [1..s] do
		c[i] := [];
		for j in [1..jmax] do
			// Compute the kij
			R, mR := RiemannRochSpace(D+j*P[i]);
			S, mS := RiemannRochSpace(D+(j-1)*P[i]);
			quot, mq := quo<R|S>;
			kji := quot.1 @@ mq @ mR;
			// Compute the bjir
			expansion, z := Expand(kji, PInf : AbsPrec := \
			rmax+1);
			coefficients, val, _ := Coefficients(expansion);
			// Fill with zeros if necessary
			bji := [Fq!0 : i in [1..(val+v)]] cat coefficients \
			cat [Fq!0 : i in [1..(rmax+2-#coefficients-val-v)]];
			// Get the cijr from the bijr
			c[i][j] := [bji[r] : r in [1..Min(v, #bji)]] \
			cat [bji[r+1] : r in [(Min(v, #bji)+1)..(rmax+1)]];
		end for;
	end for;
	return c;
end intrinsic;
\end{lstlisting}
\section{Construction of the Sequence}\label{sec:implconstruction}

Now that we have all required components for the procedure explained in Chapter \ref{ch:construction} we can take a closer look at how to implement this construction. Assume we have the bijections $\psi_r$ and $\eta_j^{(i)}$ stored in \texttt{psi[r+1]} and \texttt{eta[i][j]} and the elements $c_{j, r}^{(i)}$ stored in \texttt{c[i][j][r+1]}. We now want to construct the element $x_n^{(i)}$ for a fixed $n \geq 0$ and $i \geq 1$.\\
First we need the elements $a_r(n)$. This can easily be achieved using the \texttt{Intseq} function of Magma.
\begin{lstlisting}[frame=single]
an := Intseq(n, q, rmax+1);
\end{lstlisting}
Now the element $a_r(n)$ is stored in \texttt{an[r+1]} so we can compute the elements $y_{n, j}^{(i)}$.
\begin{lstlisting}[frame=single]
yni := [];
for j in [1..jmax] do
	sum := Fq!0;
	for r in [0..rmax] do
		sum +:= c[i][j][r+1]*psi[r+1](an[r+1]);
	end for;
	yni[j] := eta[i][j](sum);
end for;
\end{lstlisting}
Finally we have everything together to compute the $x_n^{(i)}$.
\begin{lstlisting}[frame=single]
xni := 0;
for j in [1..jmax] do
	xni := xni+yni[j]*q^(-j);
end for;
\end{lstlisting}
We can merge the computation of the $y_{n, j}^{(i)}$ and $x_n^{(i)}$ and extend it to compute all $n_\text{max}$ points as follows.
\begin{lstlisting}[frame=single]
x := [[&+[(eta[i][j](&+[c[i][j][r+1]*psi[r+1](an[r+1]): \
r in [0..rmax]]) @@ pi)*q^(-j) : j in [1..jmax]] \
: i in [1..s]] where an := Intseq(n, q, rmax+1) : n in [0..nmax]];
\end{lstlisting}
After this we have $x_n^{(i)} = \texttt{x[n+1][i]}$.\\
Using this together with the code parts above we end up with a complete algorithm that, with a global function field $K$, a feasible divisor $D$ and an integer $n_\text{max}$ as input, returns a low-discrepancy point set with $n_\text{max}$ points.
\begin{lstlisting}[frame=single]
intrinsic ConstructLDS(K::FldFun, D::DivFunElt, nmax::RngIntElt, \
s::RngIntElt, jmax::RngIntElt) -> SeqEnum
	{Returns a low-discrepancy point set with nmax+1 points.}
	// Preliminaries
	Fq := ConstantField(K);
	q := #Fq;
	B, pi := Integers(q);
	rmax := Ceiling(Log(q, nmax))+1;

	// Get Random Bijections
	psi := [GetRandomBijection(B, Fq) : r in [0..rmax]];
	eta := [[GetRandomBijection(Fq, B) : j in [1..jmax]] : i in [1..s]];

	// Find c[i][j][r+1] Elements
	c := FindCElements(K, D, s, jmax, rmax);

	// Construct the set
	x := [[&+[(eta[i][j](&+[c[i][j][r+1]*psi[r+1](an[r+1]): \
	r in [0..rmax]]) @@ pi)*q^(-j) : j in [1..jmax]] : \
	i in [1..s]] where an := Intseq(n, q, rmax+1) : n in [0..nmax]];

	return x;
end intrinsic;
\end{lstlisting}

\section{Computing the Discrepancy}\label{sec:finddiscrepancy}
Although we can compute an upper bound for the discrepancy given that we have a $(g(K), s)$-sequence (see Chapter \ref{ch:construction}), this bound is far too high for a finite set of constructed points. Since we are mostly interested in low-discrepancy point sets to use with numeric algorithms we would like to know the discrepancy for a given set of points.\\
Computing the exact discrepancy for a set of points $X = \{x_1, \ldots, x_n\}$ is not hard but quite processing intensive. Although it is defined by a supremum (cf. Definition \ref{def:discrepancy}), Lemma \ref{exactdiscrepancy} provides a way to only have to check a finite number of intervals to get the exact discrepancy. However, the number of intervals increases exponentially with the number of points $n$ and therefore this method is not practicable even for relatively small $n$. We are able to reduce the number of intervals to $n^s$ and then have to compute $A(J; n)$ for each of them in $O(n)$, but this still leaves us with a complexity of $O(n^{s+1})$.\\
Luckily, in many cases we do not need the exact discrepancy and are satisfied with an upper and lower bound for it. Finding a lower bound is not hard --- we can just compute $|D(J; n)|$ for some intervals $J \subseteq I^s$ and take the maximum. Algorithm \ref{alg:lowerdiscbound} does exactly this: First we create the boundaries for all intervals we want to check. In this case we have a unit fraction $r$ given and construct our intervals to be
$$\left\{\prod_{i=1}^s [0, j_i) \mid j_i \in \{r, 2r, \ldots, 1\} \text{ for all $i$}\right\},$$
leading to a total of $(1/r)^s$ intervals. After this we compute $|D(J; n)|$ for each of those intervals and return the maximum as a lower bound for the discrepancy.

\begin{algorithm}
	\caption{Finding a lower discrepancy bound
		\label{alg:lowerdiscbound}}
	\begin{algorithmic}[1]
		\Require{A set of $n$ points $X = \{x_1, \ldots, x_n\} \subseteq I^s$ and a unit fraction $r < 1$}
		\Ensure{A lower bound for the discrepancy $\Delta(n)$}
		\Statex
		\Function{DiscrepancyLowerBound}{$X$, $r$}
			\Let{$s$}{\textit{Dimension of the points}}
			\Let{$M$}{$\prod_{1}^s \{r, 2r, \ldots, 1\}$}
			\Let{$l$}{$0$}
			\ForAll{$(j_1, \ldots, j_s) \in M$}
				\Let{$J$}{$\prod_{i=1}^s [0, j_i)$}
				\Let{$l$}{$\max\{l, |A(J; n) - V(J)n|\}$}\label{step:calcD}
			\EndFor
			\Return{$l$}
		\EndFunction
	\end{algorithmic}
\end{algorithm}

To get a better upper bound than the trivial bound $n$ we have to think a bit more. Let $0 < r < 1$ and $J = \prod_{i=1}^s [0, j_i) \subseteq I^s$. Then we define the interval
$$J_> \coloneqq J_>(r) \coloneqq \prod_{i=1}^s [0, \min\{j_i+r, 1\}) \subseteq I^s$$
which lies in $I^s$ as well. We can compute $D(J; n)$ and $D(J_>; n)$ but there might be an interval $J \subseteq J' \subseteq J_>$ with $|D(J'; n)| > \max \{|D(J; n)|, |D(J_>; n)|\}$, so we need an upper bound for $|D(J'; n)|$.\\
Let $J \subseteq J' \subseteq J_>$. Since $J \subseteq J_>$ we obviously have $A(J) \leq A(J') \leq A(J_>)$ and $V(J) \leq V(J') \leq V(J_>)$, thus
$$A(J) - V(J_>)n \leq A(J') - V(J')n \leq A(J_>) - V(J)n.$$
At this point we know
$$|D(J'; n)| \leq \max \{A(J_>)-V(J)n, V(J_>)n-A(J)\}.$$
It is quite easy to compute $V(J)$ and $V(J_>)$. The processing intensive step is to get the value for $A(J)$ and $A(J_>)$ so it would be nice if we could recycle the results in Algorithm \ref{alg:lowerdiscbound}, Step \ref{step:calcD}, were we already computed it. Let $M$ be the set of intervals we check in this algorithm, constructed by the unit fraction $r$. By reordering we see that
$$\max_{J \in M} \{A(J_>)-V(J)n, V(J_>)n-A(J)\} = \max_{J \in M} \{A(J)-V(J_<)n, V(J_>)n-A(J)\}$$
for
$$J_< \coloneqq J_<(r) \coloneqq \prod_{i=1}^s [0, \max\{j_i-r, 0\}),$$
if $J = \prod_{i=1}^s [0, j_i)$. Using this we can extend Algorithm \ref{alg:lowerdiscbound} to also return an upper bound for the discrepancy, leading to Algorithm \ref{alg:bothdiscbound}. Now we only have a complexity of $O((1/r)^s\cdot n)$ so we will get our results faster than by computing the exact discrepancy. Of course, with larger $n$ we will have to decrease $r$ to get acceptable bounds.

\begin{algorithm}[h]
	\caption{Finding a lower and upper discrepancy bound
		\label{alg:bothdiscbound}}
	\begin{algorithmic}[1]
		\Require{A set of $n$ points $X = \{x_1, \ldots, x_n\} \subseteq I^s$ and a unit fraction $r < 1$}
		\Ensure{A tupel of lower and upper bound for the discrepancy $\Delta(n)$}
		\Statex
		\Function{DiscrepancyBounds}{$X$, $r$}
			\Let{$s$}{\textit{Dimension of the points}}
			\Let{$M$}{$\prod_{1}^s \{r, 2r, \ldots, 1\}$}
			\Let{$l$}{$0$}
			\Let{$u$}{$0$}
			\ForAll{$(j_1, \ldots, j_s) \in M$}
				\Let{$J$}{$\prod_{i=1}^s [0, j_i)$}
				\Let{$A$}{$A(J; n)$}
				\Let{$l$}{$\max\{l, |A - V(J)n|\}$}
				\Let{$J_s$}{\Call{ShrinkInterval}{$J$, $r$}}
				\Let{$J_g$}{\Call{GrowInterval}{$J$, $r$}}
				\Let{$u$}{$\max\{u, V(J_g)n-A, A-V(J_s)n\}$}
			\EndFor
			\Return{$(l, u)$}
		\EndFunction
		\Statex
		\Function{ShrinkInterval}{$J$, $r$}
			\Let{$s$}{\textit{Dimension of the interval}}
			\For{$i=1, \ldots, s$}
				\Let{$j'_i$}{$\max\{j_i - r, 0\}$}
			\EndFor
			\Return{$\prod_{i=1}^s [0, j'_i)$}
		\EndFunction
		\Statex
		\Function{GrowInterval}{$J$, $r$}
			\Let{$s$}{\textit{Dimension of the interval}}
			\For{$i=1, \ldots, s$}
				\Let{$j'_i$}{$\min\{j_i+r, 1\}$}
			\EndFor
			\Return{$\prod_{i=1}^s [0, j'_i)$}
		\EndFunction
	\end{algorithmic}
\end{algorithm}

To implement this algorithm in Magma we first have to implement some basic methods such as computation of the volume of an interval and computing the value $A(J; n)$ for a given point set. Since the starting point of the intervals is always $(0, \ldots, 0) \in I^s$ we can represent the interval $J = \prod_{i=1}^s [0, j_i)$ by a list of the $j_i$. Then we can compute the volume quite easily.
\begin{lstlisting}[frame=single]
volume := &*J;
// This yields the product of all elements of the list J
\end{lstlisting}
Shrinking and growing the interval can be implemented directly.
\begin{lstlisting}[frame=single]
Js := [Max(i-r, 0) : i in J];
Jg := [Min(i+r, 1) : i in J];
\end{lstlisting}
To check whether a given point $x = (x_1, \ldots, x_s) \in I^s$ is contained in the interval $J$ we only have to check the larger interval bounds. So computing $A(J; n)$ for a point set $X \subseteq I^s$ can be done as follows.
\begin{lstlisting}[frame=single]
A := 0;
for x in X do
	inc := 1;
	for i in [1..s] do
		if (x[i] ge J[i]) then
			inc := 0;
			break;
		end if;
	end for;
	A +:= inc;
end for;
\end{lstlisting}
Using the formal list constructor we can do this in a more mathematical way.
\begin{lstlisting}[frame=single]
A := #[x : x in X | forall{i : i in [1..#x] | x[i] lt I[i]}];
\end{lstlisting}
The last thing we need is a way to construct the set $M$ in Algorithm \ref{alg:bothdiscbound}.
\begin{lstlisting}[frame=single]
M := [[c[i] : i in [1..#c]] : c in CartesianPower([i*r : \
i in [1..1/r]], s)];
\end{lstlisting}

If we create the respective functions for the the steps shown above we can finally implement Algorithm \ref{alg:bothdiscbound}.
\begin{lstlisting}[frame=single]
intrinsic FindDiscrepancyBounds(X::SeqEnum, r::FldRatElt) -> \
FldRatElt, FldRatElt
	{Returns upper and lower bound for the discrepancy}
	l := 0;
	u := 0;
	M := [[c[i] : i in [1..#c]] : \
	c in CartesianPower([i*r : i in [1..1/r]], s)]
	for J in M do
		A := #[x : x in X | forall{i : i in [1..#x] | \
		x[i] lt J[i]}];
		l := Max(l, AbsoluteValue(A-(&*J)*n));
		Js := [Max(i-r, 0) : i in J];
		Jg := [Min(i+r, 1) : i in J];
		u, _ := Max([u, (&*Jg)*n-A, A-(&*Js)*n]);
	end for;
	return l, u;
end intrinsic;
\end{lstlisting}