%!TEX root = ../dissertation.tex

\chapter{Construction of Low-Discrepancy Sequences by Means of Function Fields} \label{ch:construction}

Now that we have the required knowledge we can look at how to construct sequences with low discrepancy. First we give a general algorithm that uses an arbitrary commutative ring $R$ with identity and finite order. This algorithm can be found in \cite{Niederreiter1} or \cite{Niederreiter2} and generates a $(t, s)$-sequence. After this we explain the special case from \cite[Section 3]{Niederreiter2} that uses a finite field as ring $R = \mathbb{F}_q$ and can effectively be implemented using function field theory.

\section{General Construction} \label{sec:construction}
In \cite{Niederreiter1} and \cite{Niederreiter2} a general algebraic construction of $(t, s)$-sequences in base $b$ is explained that we will state here.\\
Let $s \geq 1$ and $b \geq 2$ be integers and $B \coloneqq \mathbb{Z} \sslash b\mathbb{Z} \coloneqq \{0, 1, \ldots, b-1\}$ a residue system of $\mathbb{Z}/b\mathbb{Z}$. Then we need to chose:
\begin{enumerate}
	\renewcommand{\labelenumi}{(S\theenumi)}
	\item a commutative ring $R$ with identity and finite order $|R| = b$;
	\item \label{it:psi}bijections $\psi_r: B \rightarrow R$ for $r \geq 0$ with $\psi_r(0)=0$ for all sufficiently large $r$ (see the first section in Chapter \ref{ch:implementation});
	\item \label{it:eta}bijections $\eta_j^{(i)}: R \rightarrow B$ for $1 \leq i \leq s$ and $j \geq 1$;
	\item \label{it:cjri}elements $c_{j, r}^{(i)} \in R$ for $1\leq i \leq s$, $j \geq 1$ and $r \geq 0$.
\end{enumerate}
\begin{remark}
	In \cite{Niederreiter1}, for fixed $i$ and $r$ the elements $c_{j, r}^{(i)}$ are required to be zero for $j$ sufficiently large whereas in \cite{Niederreiter2} this constraint is omitted. In Lemma \ref{lem:cjri} we see that, in fact, we can work with arbitrary $c_{j, r}^{(i)}$.
\end{remark}
For $n=1, 2, \ldots$ let
$$n = \sum_{r=0}^\infty a_r(n)b^r$$
be the representation of $n$ in base $b$, $a_r(n) \in B$ for $r \geq 0$.
We define
\begin{equation}\label{eq:consy}
y_{n, j}^{(i)} \coloneqq \eta_j^{(i)} \left( \sum_{r=0}^\infty c_{j, r}^{(i)} \psi_r(a_r(n)) \right) \in B
\end{equation}
for $1 \leq i \leq s$ and $j \geq 1$. Then we can put
\begin{equation}\label{eq:consx}
x_n^{(i)} \coloneqq \sum_{j=1}^\infty y_{n, j}^{(i)} b^{-j}
\end{equation}
for $1 \leq i \leq s$ and the
\begin{equation}\label{eq:sequence}
x_n \coloneqq (x_n^{(1)}, \ldots, x_n^{(s)}) \in I^s
\end{equation}
form a sequence of points in $I^s$. For the well-definedness of the sums we refer to the fourth section of Chapter \ref{ch:implementation}, where we will also analyze the possibility to implement this as a terminating algorithm for fixed $n$. However, for this sequence to be a $(t, s)$-sequence, the elements $c_{j, r}^{(i)}$ need to satisfy some conditions, as stated in \cite[Lemma 1 and 2]{Niederreiter1}.

\section{The Case $R = \mathbb{F}_q$}
In the case $R = \mathbb{F}_q$ for a prime power $q$ we can even give the value for $t$. For $1 \leq i \leq s$ and $j \geq 1$ we can create sequences
$$c_j^{(i)} \coloneqq (c_{j, 0}^{(i)}, c_{j, 1}^{(i)}, \ldots) \in \mathbb{F}_q^\infty$$
in the sequence space over $\mathbb{F}_q$ and consider the system
$$C^{(\infty)} \coloneqq \{c_j^{(i)} \in \mathbb{F}_q^\infty \mid 1 \leq i \leq s \text{~and~} j \geq 1\}.$$
Using the projections
$$\pi_m: \mathbb{F}_q^\infty \rightarrow \mathbb{F}_q^m, (c_0, c_1, \ldots) \mapsto (c_0, c_1, \ldots, c_m)$$
for $m \geq 1$ we define $\varrho(C^{(m)})$ to be the largest integer $d$ such that any system $\{\pi_m(c_j^{(i)}) \mid 1 \leq i \leq s \text{~and~} 1 \leq j \leq d_i\}$ with $0 \leq d_i \leq m$ for $1\leq i \leq s$ and $\sum_{i=1}^s d_i = d$ is linearly independent over $\mathbb{F}_q$ (where the empty system is viewed as linearly independent). Then we can set $$\tau(C^{\infty}) \coloneqq sup_{m \geq 1} (m - \varrho(C^{(m)})).$$
Now we can state the following lemma \cite[Lemma 2]{Niederreiter2}.
\begin{lemma} \label{lem:isstsequence}
	If $R = \mathbb{F}_q$ and the elements $c_{j, r}^{(i)} \in \mathbb{F}_q$ in (S\ref{it:cjri}) are such that $\tau(C^{(\infty)}) < \infty$, then the sequence of the $x_n$ in (\ref{eq:sequence}) is a $(t, s)$-sequence constructed over $\mathbb{F}_q$ with $t = \tau(C^{(\infty)})$.
\end{lemma}
Using function fields we can state an algorithm that computes elements $c_{j, r}^{(i)} \in \mathbb{F}_q$ which satisfy the condition of Lemma \ref{lem:isstsequence} and thus give raise to a $(t, s)$-sequence.

\section{Finding $c_{j, r}^{(i)}$ Using Function Fields}\label{sec:findingc}
Now we will give an explicit way of finding the elements $c_{j, r}^{(i)}$ to finally get a low-discrepancy sequence of points in $I^s$. As ring $R$ we again use the finite field $\mathbb{F}_q$ of cardinality $q$. Additionally, we choose a function field $K/\mathbb{F}_q$ over $\mathbb{F}_q$ of genus $g(K) > 0$ with at least $s+1$ rational places $P_\infty, P_1, \ldots, P_s$ and a positive divisor $D \in \mathcal{D}_K$ with $\deg D = g(K)$ and $l(D) = 1$. We fix a uniformizer $z$ of the infinite place $P_\infty$ and set $v \coloneqq \ord_{P_\infty} D$.
\begin{lemma}\label{lem:findk}
	For any rational place $P \in \{P_1, \ldots, P_s\}$ and integer $n \geq 0$ there exists an element $k \in K$ with $\ord_p k = -\ord_p D - n$.
\end{lemma}
\begin{proof}
	In Lemma \ref{lem:dimension} we have seen that for any rational place $P \in \mathbb{P}_K$ and integer $n \geq 0$ the dimension $l(D + nP)$ equals to $l(D) + n$. In particular, this means that $\mathcal{L}(D + nP) \setminus \mathcal{L}(D + (n - 1)P) \neq \emptyset$ and for an element $k \in K$ to lie in $\mathcal{L}(D + nP)$ but not in $\mathcal{L}(D + (n - 1)P)$ its valuation $\ord_P k$ has to be $-\ord_P D - n$.
\end{proof}
The proof even shows that an infinite number of such elements exists.\\
To get the $c_{j, r}^{(i)}$ we have to do the following:
\begin{itemize}
	\item For $1 \leq i \leq s$ and $j \geq 1$ we choose elements $k_j^{(i)} \in K$ with 
	$$\ord_{P_i} k_j^{(i)} = -\ord_{P_i} D - j.$$
	\item Since $\ord_{P_\infty} k_j^{(i)} \geq - \ord_{P_\infty} D$ we have the expansion
	\begin{equation}\label{eq:kji}
	k_j^{(i)} = z^{-v} \sum_{r=0}^\infty b_{j, r}^{(i)} z^r
	\end{equation}
	with $b_{j, r}^{(i)} \in \mathbb{F}_q$ for $1 \leq i \leq s$, $j \geq 1$, and $z$ the fixed uniformizer of $P_\infty$.
	\item Finally we can define
	$$c_{j, r}^{(i)} = \begin{cases} b_{j, r}^{(i)} & \text{for } 0 \leq r \leq v-1,\\
									 b_{j, r+1}^{(i)} & \text{for } r \geq v. \end{cases}$$
\end{itemize}

These $c_{j, r}^{(i)}$ then can be used in (S\ref{it:cjri}) to create a point sequence in $I^s$ as explained in the first section. Moreover, the sequence descending from these elements is an $(s, t)$-sequence with $t \leq g(K)$ as can be shown by the following theorem in connection with Lemma \ref{lem:isstsequence}.
\begin{theorem}
	For the $c_{j, r}^{(i)}$ chosen as above we have
	$$\tau(C^{(\infty)}) \leq g(K).$$
\end{theorem}
\begin{proof}
	\cite[Theorem 1]{Niederreiter2}.
\end{proof}

The next question that arises is whether we can always find such a divisor $D$ as required above, that is $\deg D = g(K)$ and $l(D) = 1$. Luckily, for most function fields this is true, as the following lemma shows.
\begin{lemma}
	There exists a positive divisor $D$ of $K/\mathbb{F}_q$ with $\deg D = g(K)$ and $l(D) = 1$ if either $q \geq 3$ and $K/\mathbb{F}_q$ has at least two rational places of degree one, or $q = 2$ and $K / \mathbb{F}_q$ has at least four rational places of degree one.
\end{lemma}
\begin{proof}
	\cite[Lemma 6]{Niederreiter2}.
\end{proof}

For a deeper analysis of the existence of low-discrepancy sequences for a given value for $t$ under different base fields $\mathbb{F}_q$ we refer to \cite{Niederreiter2}. We will concentrate more on the practical aspect of implementing the algorithm above and examining the generated sequences.\\
We conclude this chapter with an example where we will construct ten points in the two dimensional interval $I^2$ using the procedure described above. We will only specify the points until $j = 3$.
\begin{example}
	We choose $R = F = \mathbb{F}_7$ and therefore $B = \mathbb{Z}\sslash 7\mathbb{Z}$.
	\begin{itemize}
		\item For the bijections in (S\ref{it:psi}) we choose the ones defined in Table \ref{tab:psi}. For $\psi_r$ with $r > 2$ it is enough to specify $\psi_r(0) = 0$ as we will see later.
		\begin{table}
			\centering
			\begin{tabular}{r|l|l|l}
				$x \in B$ & $\psi_0(x)$ & $\psi_1(x)$ & $\psi_2(x)$ \\
				\hline
				$0$ & $3$ & $4$ & $3$ \\
				$1$ & $2$ & $1$ & $2$ \\
				$2$ & $5$ & $3$ & $6$ \\
				$3$ & $0$ & $2$ & $4$ \\
				$4$ & $1$ & $0$ & $1$ \\
				$5$ & $6$ & $6$ & $5$ \\
				$6$ & $4$ & $5$ & $0$
			\end{tabular}
			\caption{The specified bijections $\psi_r : B \rightarrow \mathbb{F}_7$.}
			\label{tab:psi}
		\end{table}
		\item The bijections $\eta_j^{(i)}$ for $1 \leq i \leq 2$, $1 \leq j \leq 3$ are presented in Table \ref{tab:eta}.
		\begin{table}
			\centering
			\begin{tabular}{r|l|l|l|l|l|l}
				$x \in B$ & $\eta_1^{(1)}(x)$ & $\eta_1^{(2)}(x)$ & $\eta_1^{(3)}(x)$ & $\eta_2^{(1)}(x)$ & $\eta_2^{(2)}(x)$ & $\eta_2^{(3)}(x)$\\
				\hline
				$0$ & $4$ & $0$ & $6$ & $1$ & $4$ & $6$ \\
				$1$ & $3$ & $2$ & $2$ & $5$ & $0$ & $2$ \\
				$2$ & $1$ & $0$ & $0$ & $3$ & $6$ & $3$ \\
				$3$ & $6$ & $3$ & $3$ & $0$ & $2$ & $4$ \\
				$4$ & $2$ & $4$ & $4$ & $6$ & $5$ & $5$ \\
				$5$ & $5$ & $5$ & $5$ & $2$ & $3$ & $1$ \\
				$6$ & $0$ & $1$ & $1$ & $4$ & $1$ & $0$
			\end{tabular}
			\caption{The specified bijections $\eta_j^{(i)} : \mathbb{F}_7 \rightarrow B$.}
			\label{tab:eta}
		\end{table}
		\item Now we have to find the elements $c_{j, r}^{(i)} \in \mathbb{F}_7$. As function field over $\mathbb{F}_7$ we take the rational function field $K = \mathbb{F}_7(x)$. The genus of $K$ is $0$ so the only positive divisor $D$ of degree $g(K)$ that we can choose is the zero divisor. The elements $k_j^{(i)}$ that we chose are listed in Table \ref{tab:kij}.
		\begin{table}
			\centering
			\begin{tabular}{c|lll}
				\diagbox{$i$}{$j$} & $1$ & $2$ & $3$ \\
				\hline
				$1$ & $1/x$ & $1/x^2$ & $1/x^3$\\
				$2$ & $5x/(x+4)$ & $4x^2/(x^2+x+2)$ & $6x^3/(x^3+5x^2+6x+1)$
			\end{tabular}
			\caption{The elements $k_j^{(i)}$ with $\ord_{P_i} k_j^{(i)} = -\ord_{P_i} D - j = - j$.}
			\label{tab:kij}
		\end{table}
		Using $v = \ord_{P_\infty} D = 0$ these lead to the elements $b_{j, r}^{(i)}$ (Table \ref{tab:bijr})
		\begin{table}
			\centering
			\begin{tabular}{c|lll}
				\diagbox{$i$}{$j$} & $1$ & $2$ & $3$ \\
				\hline
				$1$ & $(0, 1, 0, 0)$ & $(0, 0, 1, 0)$ & $(0, 0, 0, 0)$ \\
				$2$ & $(5, 1, 3, 0)$ & $(4, 3, 3, 0)$ & $(6, 5, 2, 0)$
			\end{tabular}
			\caption{The elements $b_j^{(i)} \coloneqq (b_{j, 0}^{(i)}, \ldots, b_{j, 3}^{(i)})$.}
			\label{tab:bijr}
		\end{table}
		and in this case they represent exactly the elements $c_{j, r-1}^{(i)}$ for $r \geq 1$ so we get Table \ref{tab:cijr}.
		\begin{table}
			\centering
			\begin{tabular}{r|lll}
				\diagbox{$i$}{$j$} & $1$ & $2$ & $3$ \\
				\hline
				$1$ & $(1, 0, 0)$ & $(0, 1, 0)$ & $(0, 0, 0)$ \\
				$2$ & $(1, 3, 0)$ & $(3, 3, 0)$ & $(5, 2, 0)$
			\end{tabular}
			\caption{The elements $c_j^{(i)} \coloneqq (c_{j, 0}^{(i)}, \ldots, c_{j, 2}^{(i)})$.}
			\label{tab:cijr}
		\end{table}
		Although we only have the $c_{j, r}^{(i)}$ for $0 \leq r \leq 2$ this suffices here as we will see in the next step.
	\end{itemize}
	Now that we have all required parts we can construct our points. We will do this in detail for $n=9$, the other points can be computed analogously.\\
	We have $9 = 2 \cdot 7^0 + 1 \cdot 7^1$ so we get $a_0(9) = 2$, $a_1(9) = 1$ and $a_r(9) = 0$ for $r > 1$. The element $y_{9, 1}^{(1)}$ then is given by
	\begin{align*}
	y_{9, 1}^{(1)} &= \eta_1^{(1)} \left( \sum_{r=0}^\infty c_{1, r}^{(1)} \psi_r(a_r(9)) \right) \\
					&\stackrel{\ast}{=} \eta_1^{(1)} \left( \sum_{r=0}^2 c_{1, r}^{(1)} \psi_r(a_r(9)) \right) \\
					&= \eta_1^{(1)} \left( c_{1, 0}^{(1)} \psi_0(a_0(9)) + c_{1, 1}^{(1)} \psi_1(a_1(9)) + c_{1, 2}^{(1)} \psi_2(a_2(9)) \right) \\
					&= \eta_1^{(1)} \left( c_{1, 0}^{(1)} \psi_0(2) + c_{1, 1}^{(1)} \psi_1(1) + c_{1, 2}^{(1)} \psi_2(0) \right) \\
					&= \eta_1^{(1)} \left(1 \cdot 5 + 0 \cdot 1 + 0 \cdot 3 \right) \\
					&= \eta_1^{(1)} (5) \\
					&= 5
	\end{align*}
	where $\stackrel{\ast}{=}$ holds because $a_r(9) = 0$ and $\psi_r(0) = 0$ for all $r > 2$. Here we also see that for our $n$ not larger than $48$ we do not need additional $c_{j, r}^{(i)}$. The same computation for $j=2, 3$ leads to $y_{9, 2}^{(1)} = 5$ and $y_{9, 3}^{(1)} = 6$ so we get
	\begin{align*}
	x_9^{(1)} &= y_{9, 1}^{(1)}7^{-1} + y_{9, 2}^{(1)}7^{-2} + y_{9, 3}^{(1)}7^{-3} \\
			  &= 286/343.
	\end{align*}
	If we do this for all $n=0, \ldots, 9$, $i=1, 2$ and $j=1, \ldots, 3$ we get the points that are shown in table \ref{tab:x}.
	\begin{table}
		\centering
		\begin{tabular}{r|l}
			$n$ & $x_n$ \\
			\hline
			$0$ & $(307/343, 276/343)$ \\
			$1$ & $(62/343, 89/343)$ \\
			$2$ & $(258/343, 8/343)$ \\
			$3$ & $(209/343, 121/343)$ \\
			$4$ & $(160/343, 4/7)$ \\
			$5$ & $(13/343, 340/343)$ \\
			$6$ & $(111/343, 167/343)$ \\
			$7$ & $(335/343, 221/343)$ \\
			$8$ & $(90/343, 141/343)$ \\
			$9$ & $(286/343, 40/49)$
		\end{tabular}
		\caption{Generated points, specified up to $7^{-3}$.}
		\label{tab:x}
	\end{table}
	As we can see these points obviously lie on a lattice with lattice width $7^{-3}$. Increasing the maximal $j$ in our computation decreases the lattice width until eventually (in theory, for $j \rightarrow \infty$) we end up with real numbers as coordinates. Figure \ref{fig:1000points} shows $1000$ points on a width-$7^{-15}$ lattice that were generated by the same function field. The discrepancy of this set is $5.807 \leq \Delta(1000) \leq 12.777$ which means those points are quite uniformly distributed. For more examples on low-discrepancy sets and their discrepancy the reader is referred to Chapter \ref{ch:results} where also a comparison to pseudo random numbers is made.\\
	\begin{figure}
		\centering
		\includegraphics[width=\linewidth]{figures/1000points}
		\captionof{figure}{$1000$ points, automatically generated by the implementation in Chapter \ref{ch:implementation}.}
		\label{fig:1000points}
	\end{figure}
	In the next chapter we will see how we can implement this algorithm to automate the process and easily receive arbitrary numbers of points.
\end{example}