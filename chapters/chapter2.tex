%!TEX root = ../dissertation.tex
%\begin{savequote}[75mm]
%This is some random quote to start off the chapter.
%\qauthor{Firstname lastname}
%\end{savequote}

\chapter{Fundamentals of Function Field Theory}\label{ch:functionfields}

In this chapter the most basic and important definitions and theorems of function field theory will be stated. For most proofs we refer to other literature such as \cite{Rosen1} or \cite{Stichtenoth1}. We begin with the introduction of valuations and valuation rings to later define function fields and places. We end this chapter with the definition of Riemann-Roch spaces, the important Riemann-Roch theorem and a preparative lemma for the construction in Chapter \ref{ch:construction}.

\section{Valuation Rings}

First we need an understanding of (discrete) valuation rings. For a deeper study of valuated fields and more examples the reader is referred to \cite{Fesenko1} or \cite{Dummit1}.

\begin{definition}\label{def:valuation}\index{Valuation}
	Let $K$ be a field and set $\mathbb{Z}_\infty \coloneqq \mathbb{Z} \cup \{\infty\}$. A map $\nu : K \rightarrow \mathbb{Z}_\infty$ that satisfies
	\begin{align}
		&\nu(x \cdot y) = \nu(x) + \nu(y)\label{eq:val1}\\
		&\nu(x + y)	   \geq \min\{\nu(x), \nu(y)\}\\
		&\nu(x)=\infty  \Leftrightarrow x = 0
	\end{align}
	for all $x, y \in K$ is called a \emph{discrete valuation} with \emph{value group} $\nu(K^\times)$.
\end{definition}
\begin{remark}
	From (\ref{eq:val1}) it follows that $\nu(1) = 0$ and therefore $\nu(x^{-1}) = -\nu(x)$.
\end{remark}
\begin{example} \leavevmode \label{ex:valuation}
	\begin{enumerate}
		\item For any field $K$ we have the \emph{trivial valuation} $\nu : K \rightarrow \mathbb{Z}_\infty$ with $\nu(x) = 0$ for $x \neq 0$, and $\nu(0) = \infty$. 
		\item \label{ex:padic} For a prime $p \in \mathbb{Z}$ let $\nu_p : \mathbb{Z} \rightarrow \mathbb{Z}_\infty$ such that $\nu_p(p^ja) = j$ for all $a \in \mathbb{Z} \setminus p\mathbb{Z}$, $j \in \mathbb{N}_0$ and $\nu_p(0)=\infty$. If we extend $\nu_p$ to $\mathbb{Q}$ via $\nu_p(a/b) \coloneqq \nu_p(a)-\nu_p(b)$ we get the \emph{$p$-adic valuation}.\index{Valuation!$p$-adic@\textsl{$p$-adic}}\\
		For example, for $p=7$ we get $\nu_7(14)=\nu_7(2 \cdot 7)=1$, $\nu_7(25)=0$ and $\nu_7(99/3430)=\nu_7((3^2 \cdot 11)/(2 \cdot 5 \cdot 7^3))=-3$.
		\item \label{ex:infval} For a field $K$ we get a valuation for its rational function field $K(x)$ via $\nu_\infty(f/g) = \deg g - \deg f$ for $f, g \in K[x]$. It is easy to see that all conditions in Definition \ref{def:valuation} are fulfilled.
	\end{enumerate}
\end{example}

\begin{definition}\index{Valuation!ring}
	A principal ideal domain $R$ with exactly one non-zero maximal ideal is called a \emph{discrete valuation ring}. For a field $K$ a discrete valuation ring $R \subseteq K$ such that for every $x \in K$ we have $x \in R$ or $x^{-1} \in R$ is called a \emph{discrete valuation ring} for $K$.
\end{definition}

As the following theorem shows, there is a strong connection between valuations and valuation rings as one can be constructed given the other. So any non-trivial valuation gives rise to a valuation ring and vice versa.

\begin{theorem}\label{thm:valvalring}
	Let $R$ be a discrete valuation ring and $\nu$ a non-trivial discrete valuation on a field $K$. Then:
	\begin{enumerate}
		\item Let $M \subseteq R$ be the unique maximal ideal in $R$ and $t$ a uniformizer, so every element $x \neq 0$ of $R$ can be uniquely written in the form $x = u_xt^{k_x}$, for $u_x \in R$ a unit and $k_x \geq 0$. Then the natural extension of $\nu_R(x) \coloneqq k_x$, $\nu_R(0) \coloneqq \infty$ to $\Quot(R)$ is a discrete valuation.
		\item $\mathcal{O}_\nu \coloneqq \{x \in K \mid \nu(x) \geq 0\}$ is a discrete valuation ring.
	\end{enumerate}
\end{theorem}
\begin{proof}
	\begin{enumerate}
		\item This part is easy to check so we will omit it here.
		\item It is easy to see that $\mathcal{O}_\nu$ is in fact a ring. We show that $m_K \coloneqq \{x \in K \mid \nu(x) > 0\} \subseteq \mathcal{O}_\nu$ is the unique maximal ideal. It is easy to see that $m_K$ is an ideal of $\mathcal{O}_\nu$ so we only have to show the maximality. Let $x \in \mathcal{O}_\nu \setminus m_K$. Then $\nu(x) = 0$ and for $x^{-1}\in K$ we have $\nu(x^{-1}) = -\nu(x) = 0$, so $x^{-1}$ also lies in $\mathcal{O}_\nu$. Thus, $x$ is a unit in $\mathcal{O}_\nu$ and we get $\mathcal{O}_\nu^\times = \mathcal{O}_\nu\setminus m_K$. So all ideals of $\mathcal{O}_\nu$ lie in $m_K$ and $m_K$ is an ideal itself, so the claim follows.
	\end{enumerate}
\end{proof}

\begin{example}
	We will continue Example \ref{ex:valuation} and find the valuation rings for the valuations described there.
	\begin{enumerate}
		\item If we take the trivial valuation $\nu$ on a field $K$ we get $\mathcal{O}_\nu = K$. However, since $K$ is a field it only has the two trivial ideals so it cannot be a valuation ring. Therefore we see that the requirement for the valuation to be non-trivial in Theorem \ref{thm:valvalring} cannot be canceled out.
		\item For the $p$-adic valuation\index{Valuation!$p$-adic@\textsl{$p$-adic}} we get the valuation ring $\mathcal{O}_{\nu_p} = \{x \in \mathbb{Q} \mid \nu_p(x) \geq 0\}$. The irreducible fractions $x/y \in \mathbb{Q}$ with $\nu_p(x/y) \geq 0$ are exactly those whose denominators are not divisible by $p$, thus $\mathcal{O}_{\nu_p} = \{x/y \mid x \in \mathbb{Z}, y \in \mathbb{Z}\setminus p\mathbb{Z}\}$. This is by definition the localization of $\mathbb{Z}$ at $p$ and we get $\mathcal{O}_{\nu_p} = \mathbb{Z}_{(p)}$.
		\item The valuation ring of $\nu_\infty$ is $\mathcal{O}_{\nu_\infty} = \{f \in K(x) \mid \nu_\infty(f) \geq 0\}
		= \{f/g \in K(x) \mid \deg(g) \geq \deg(f)\}$.
	\end{enumerate}
\end{example}

\section{Function Fields, Places and Divisors}

Now that we have an understanding of valuation rings we can define function fields and places. At the end of this section we will have all tools to understand the construction in the following chapter.

\begin{definition}\index{Function field}
	Let $F$ be a field and $K/F$ a field extension. If there is a transcendental $x \in K \setminus F$ such that the degree $[K:F(x)]$ is finite, we call $K/F$ a \emph{function field} over F.\\
	If, in addition, $F$ is finite we call it \emph{global function field}.
\end{definition}
\begin{lemma}
	Let $K/F$ be a function field. Then $E := \{x \in K \mid \text{$x$ is algebraic over $F$} \}$ is a field with $[E : F] < \infty$. We call it the \emph{constant field} of the function field $K$.
\end{lemma}
\begin{proof}
	See \cite[Corollary 1.1.16]{Stichtenoth1}.
\end{proof}

Without loss of generality, in this thesis we will assume for any function field $K/F$ that $F$ is the \emph{full constant field} of $K$, that is, $F=E$.

\begin{example} \leavevmode
	\begin{enumerate}
		\item For a field $F$ the rational function field $F(x)$ is in fact a function field.
		\item $\mathbb{F}_{11}(x)[y]/(y^3+y+x^5+x+1)$ is a function field with constant field $\mathbb{F}_{11}$.
	\end{enumerate}
\end{example}

\begin{definition}\index{Place}\index{Prime|see {Place}}\index{Place!Rational Place}
	Let $K/F$ be a function field. A discrete valuation ring $F \subseteq R \subseteq K$ of $K$ is called \emph{place} or \emph{prime} of $K$. Write
	$$\mathbb{P}_K \coloneqq \mathbb{R}_{K/F} \coloneqq \{R \mid \text{$R$ is prime of $K$}\}$$
	for the set of primes of $K$.\\
	For a prime $R$ of $K$ let $P \subseteq R$ be the unique maximal ideal in $R$ and $\ord_P : K = \Quot R \rightarrow \mathbb{Z}_\infty$ the valuation on $K$ induced by $R$ (see theorem \ref{thm:valvalring}). Then $R = \{x \in K \mid \ord_P(x) \geq 0\}$ and $P = \{x \in K \mid \ord_P(x) > 0\}$.\\
	We define the \emph{degree} of $P$ to be $\deg P \coloneqq [R/P : F]$. A place of degree $1$ is called a \emph{rational place}.\\
	Let $a \in K$ and $R \in \mathbb{P}_{K/F}$ with maximal ideal $P$. If $\ord_P(a) > 0$ we call $R$ a \emph{root}\index{Root} or \emph{zero} of multiplicity $\ord_P(a)$ of $a$ and if $\ord_P(a) < 0$ a \emph{pole}\index{Pole} of multiplicity $-\ord_P(a)$.
\end{definition}

\begin{remark}
	Since there is a one-to-one correspondence between a prime $R$ of a function field $K/F$ and its unique maximal ideal $P$, we will also call $P$ a prime\index{Place} of $K/F$. We then denote by $R_P \coloneqq R$ its corresponding valuation ring.
\end{remark} 

\begin{lemma}
	The degree $\deg P$ of a place $P$ of a function field $K/F$ is finite.
\end{lemma}
\begin{proof}
	See \cite[Proposition 1.1.15]{Stichtenoth1}.
\end{proof}


\begin{example} \leavevmode \label{ex:rationalff}
	Let $F$ be a field and $K = F(x)$ the rational function field over $F$. Then every non-zero prime ideal in $A = F[x]$ is generated by a unique monic irreducible polynomial $p$ and we get a discrete valuation ring $A_{(p)}$ if we localize $A$ at $p$. Obviously $F \subseteq A_{(p)}$ and since $F[x] = A \subseteq A_{(p)}$, $\Quot F[x] = F(x) = K$ we also have $\Quot A_{(p)} = K$, thus making $A_{(p)}$ a prime of $K$. We denote it by $P_p \coloneqq A_{(p)}$. Its maximal ideal is $\{a \in K \mid \ord{P_p} (a) > 0\} = \{f/g \in K \mid f \in pF[x], g \in F[x]\setminus pF[x]\} = pA_{(p)}$ so we get $\deg P_p = [A_{(p)}/pA_{(p)} : F] = [F[x]/pF[x] : F] = \deg(p)$.\\
	In fact, there is only one prime of $K$ that cannot directly be derived by such a polynomial of $F[x]$ (see \cite[Theorem 1.2.2]{Stichtenoth1}). Instead, we take $A'=F[x^{-1}]$. Here the polynomial $x^{-1}$ is prime and the localization $A'_{(x^{-1})}$ is also a prime of $K$. It is called the \emph{prime at infinity}\index{Place!at infinity} and usually denoted by $P_\infty$. The valuation $\ord_{P_\infty}$ (cf. Example \ref{ex:valuation}.\ref{ex:infval}) sends a polynomial $g \in F[x]$ to its negative degree, $\ord_{P_\infty} g = -\deg g$, and thus for a rational function $f = g/h \in K$, $g, h \in F[x]$ we have $\ord_{P_\infty} f = \deg h - \deg g$.\\
	Now let $F=\mathbb{F}_{11}$, $K=F(x)$ and $f = (x^5+3x^4+6x^2+8x)/(x^4+6x^3+7x^2+8) \eqqcolon g/h \in K$, $g, h \in F[x]$. The numerator of $f$ factorizes as $g = x(x+1)^2(x^2+x+8)$ and the denominator as $h = (x+10)(x^3+7x^2+3x+3)$. In Table \ref{tab:ordex} you can see the different valuations for f. Note that $\ord_{P_p} f = 0$ for every other monic irreducible $p \in F[x]$.
	\begin{table}
		\centering
		\begin{tabular}{r||l|l|l|l|l|l}
			$p \in F[x] \cup \{\infty\}$ & $x$ & $x+1$ & $x+10$ & $x^2+x+8$ & $x^3+7x^2+3x+3$ & $\infty$ \\
			\hline
			$\ord_{P_p} f$				 & $1$ & $2$   & $-1$   & $1$       & $-1$            & $-1$
		\end{tabular}
		\caption{The valuations for $f=(x^5+3x^4+6x^2+8x)/(x^4+6x^3+7x^2+8)$.}
		\label{tab:ordex}
	\end{table}
\end{example}

\begin{theorem}
	Every function field has infinitely many primes.
\end{theorem}
\begin{proof}
	This theorem follows from the \emph{Weak Approximation Theorem}. We will not need this here, so we refer to \cite[Theorem 1.3.1, Corollary 1.3.2]{Stichtenoth1} for its statement and proof.
\end{proof}

\begin{definition}\index{Divisor}\index{Divisor!group}
	Let $K/F$ be a function field. The free abelian group
	$$\mathcal{D}_K \coloneqq \bigoplus_{P \in \mathbb{P}_K} \mathbb{Z}$$
	generated by the primes of $K$ is called the \emph{group of divisors}. Elements $D \in \mathcal{D}_K$ are called \emph{divisors} and written additively as
	$$D = \sum_{P \in \mathbb{P}_K} a_P P$$
	with almost all $a_P = 0$.\\
	For a divisor $D = \sum_{P \in \mathbb{P}_K} a_P P$ we define its \emph{degree} $\deg D \coloneqq \sum_{P \in \mathbb{P}_K} a_P \deg P$ and denote its coefficients as $\ord_P D \coloneqq a_P$. A divisor $D$ with $\ord_P(D) \geq 0$ for all $P \in \mathbb{P}_K$ is called \emph{effective} or \emph{positive}. We then write $D \geq 0$.\\
\end{definition}

Similar to ideals we can get a principal divisor from any element $a \in K^\times$ of a number field $K$.
\begin{definition} \label{def:principaldivisor}\index{Divisor!principal@\textsl{principal}}
	Let $K/F$ be a number field and $a \in K^\times$. Then we define the \emph{principal divisor} generated by $a$ to be
	\begin{equation}\label{eq:principaldivisor}
		(a) \coloneqq \sum_{P \in \mathbb{P}_K} \ord_P(a)P \in \mathcal{D}_K
	\end{equation}
	and set $\mathcal{P}_K = \{(a) \mid a \in K^\times\}$ to be the group of principal divisors. We call
	\begin{equation*}
	\begin{split}
	(a)_0 & \coloneqq \sum_{\substack{P \in \mathbb{P}_K\\ \ord_P(a) > 0}} \ord_P(a)P \\
	(a)_\infty & \coloneqq \sum_{\substack{P \in \mathbb{P}_K\\ \ord_P(a) < 0}} -\ord_P(a)P
	\end{split}
	\end{equation*}
	the \emph{zero} or \emph{pole divisor} and \emph{root divisor} of $a$, respectively.
\end{definition}

\begin{example}\label{ex:principaldivisor}
	Let's look again at the end of Example \ref{ex:rationalff}, so we have $F=\mathbb{F}_{11}$, $K=F(x)$ and $f = (x^5+3x^4+6x^2+8x)/(x^4+6x^3+7x^2+8) \in K$. As we can see in Table \ref{tab:ordex} for the principal divisor generated by $f$ we get
	$$(f) =  P_x + 2P_{x+1} - P_{x+10} + P_{x^2+x+8} - P_{x^3+7x^2+3x+3} - P_\infty$$
	with a pole and root divisor of
	\begin{align*}
	(f)_0 &= P_x + 2P_{x+1} + P_{x^2+x+8}\\
	(f)_\infty &= P_{x+10} + P_{x^3+7x^2+3x+3} + P_\infty.
	\end{align*} 
\end{example}

Now we will show that Definition \ref{def:principaldivisor} is also well defined in the general case, that is, the sum in (\ref{eq:principaldivisor}) is finite.
\begin{lemma}
	Let $K/F$ be a function field and $a \in K^\times$. Then $\ord_P a = 0$ for all but finitely many $P \in \mathbb{P}_K$.
\end{lemma}
\begin{proof}
	We differentiate two cases:
	\begin{enumerate}
		\item $a \in F^\times$: Since $F^\times \subseteq F \subseteq R_P$ is contained in the valuation ring for every prime $P$, also $a^{-1} \in R_P$, hence $a$ is a unit in $R_P$. Therefore $a \notin P$, so $a \in R_P \setminus P$ and $\ord_P a = 0$ for every $P$.
		\item $a \notin F^\times$: In this case $a$ is transcendental and the number of zeros of $a$ is $\leq [K : F(x)]$ (cf. \cite[Proposition 1.3.3]{Stichtenoth1}). The same holds for the number of poles of $a$ that is equal to the number of zeros of $a^{-1} \notin F^\times$.
	\end{enumerate}
\end{proof}

\begin{theorem}
	For all $a \in K^\times$ the degree $\deg (a)$ of the principal divisor $(a)$ is always zero.
\end{theorem}
\begin{proof}
	\cite[Theorem 1.4.11]{Stichtenoth1}.
\end{proof}

\begin{definition}\index{Divisor!class group}
	Two divisors $D_1$ and $D_2$ are called \emph{linearly equivalent} (write $D_1 \sim D_2$) if there exists a principal divisor $(a)$, $a \in K^\times$, such that $D_1 - D_2 = (a)$. The group of divisors modulo linear equivalence $\mathcal{C}l_K = \mathcal{D}_K/\mathcal{P}_K$ is called the \emph{divisor class group}.
\end{definition}

Using principal divisors we introduce Riemann-Roch spaces which we will need later to construct our sequences.
\begin{definition}\index{Riemann-Roch!space}
	 For a divisor $D$ we define the \emph{Riemann-Roch space} of $D$ to be
	 $$\mathcal{L}(D) \coloneqq \{a \in K^\times \mid (a) + D \geq 0 \} \cup \{0\}$$
	 and its dimension $l(D) \coloneqq \dim D \coloneqq \dim \mathcal{L}(D)$.
\end{definition}

\begin{lemma}
	$\mathcal{L}(D)$ is an $F$-vector space.
\end{lemma}
\begin{proof}
Let $a, b \in \mathcal{L}(D)$. Then for all $P \in \mathbb{P}_K$ it holds that $\ord_P a, \ord_P b \geq -\ord_P D$. But then $\ord_P (a + b) \geq \min \{\ord_P a, \ord_P b\} \geq -\ord_P D$, thus $a + b \in \mathcal{L}(D)$. Additionally, for all $\lambda \in F$ we have $\ord_P (\lambda a) = \ord_P \lambda + \ord_P a = \ord_P a \geq -\ord_P D$ so also $\lambda a \in \mathcal{L}(D)$.
\end{proof}

\begin{lemma}\label{lem:dimensions}
	Let $D_1, D_2 \geq 0 \in \mathcal{D}_K$ two divisors of $K$ with $D_1 \leq D_2$. Then $\mathcal{L}(D_1) \subseteq \mathcal{L}(D_2)$ and
	$$\dim (\mathcal{L}(D_2)/\mathcal{L}(D_1)) \leq \deg D_2 - \deg D_1.$$
\end{lemma}
\begin{proof}
	See \cite[Lemma 1.4.8]{Stichtenoth1}.
\end{proof}

\begin{lemma}
	The dimension of the Riemann-Roch space $\mathcal{L}(D)$ is finite.
\end{lemma}
\begin{proof}
	We split up $D$ in its effective and non-effective part $D = D_+ - D_-$ with $D_+, D_- \geq 0$. In Lemma \ref{lem:dimensions} set $B = D_+$ and $A = 0$ the zero divisor. Since $\mathcal{L}(0) = K$ we get $\dim \mathcal{L}(D_+) \leq \deg D_+ + 1$ and therefore $\dim \mathcal{L}(D) \leq \dim \mathcal{L}(D_+) < \infty$.
\end{proof}

\begin{example}
	We start with the same setting as in Example \ref{ex:principaldivisor}, that is, we have $F=\mathbb{F}_{11}$, $K=F(x)$ and $f = (x^5+3x^4+6x^2+8x)/(x^4+6x^3+7x^2+8) \in K$. For the principal divisor $(f)$ we get $l((f)) = 1$ and $\{(x^4+6x^3+7x^2+8)/(x^5+3x^4+6x^2+8x)\} = \{f^{-1}\}$ is a basis of $\mathcal{L}((f))$.\\
	For the divisor $D \coloneqq P_\infty - 2P_{x+7} + P_{x^2+7x+2}$ we get $l(D) = 2$ and a basis of $\mathcal{L}(D)$ is given by $\{x(x+7)^2/(x^2+7x+2), (x+7)^2/(x^2+7x+2)\}$.
\end{example}

\begin{theorem}[Riemann-Roch]\label{thm:riemannroch}\index{Riemann-Roch!theorem}
	There is an integer $g \geq 0$ and a divisor class $\mathcal{C} \in \mathcal{C}l_K$ such that for all divisors $C \in \mathcal{C}$ and $D \in \mathcal{D}_K$ we have
	$$l(D) = \deg D - g + 1 + l(C - D).$$
\end{theorem}
The proof of this theorem needs additional definitions (such as \emph{Weil differentials}) and lemmas so we will omit it here and refer to \cite[Theorem 5.4 ff]{Rosen1}.
\begin{remark}
	The integer $g$ in Theorem \ref{thm:riemannroch} is uniquely determined by $K$ and called \emph{genus} of $K$. We write $g(K)$ or $g(K/F)$ and it holds that $g(K) = \max\{\deg D - l(D) + 1 \mid D \in \mathcal{D}_K\}$. The divisor class $\mathcal{C}$ is also uniquely determined and called the \emph{canonical class}.
\end{remark}

Using that $l(C - D) \geq 0$ for any $D$ and $C$ we get the \emph{Riemann-Inequality}\index{Riemann-Inequality} as a simple corollary of the theorem.
\begin{corollary}[Riemann-Inequality]
	For all divisors $D \in \mathcal{D}_K$ it holds that
	$$l(D) \geq \deg D - g(K) + 1.$$
\end{corollary}

To use Riemann-Roch spaces for creating low-discrepancy sequences we finally have to show the following lemma.
\begin{lemma} \label{lem:dimension}
	Let $D$ be a divisor of a global function field $K/\mathbb{F}_q$ with degree $\deg D = g(K/\mathbb{F}_q)$ and dimension $l(D) = 1$. Then for any integer $n \geq 0$ and rational place $P \in \mathbb{P}_K$ of $K$ we have
	$$l(D + nP) = n + 1.$$
\end{lemma}
\begin{proof} \leavevmode
	\begin{itemize}
		\item $l(D + nP) \geq n + 1$: This follows directly from the Riemann-Inequality.
		\item $l(D + nP) \leq n + 1$: Using Lemma \ref{lem:dimensions} with $D_2 = D + nP$ and $D_1 = D$ we get
		\begin{align*}
		l(D + nP) - 1 &= l(D + nP) - l(D)\\
					  &= \dim (\mathcal{L}(D + nP)/\mathcal{L}(D))\\
					  &\leq \deg (D + nP) - \deg D\\
					  &= \deg nP\\
					  &= n,
		\end{align*}
		thus proving the claim.
	\end{itemize}
\end{proof}