%!TEX root = ../dissertation.tex

\chapter{Results}\label{ch:results}

In this chapter we will look at different point sets that were created by the algorithm in Chapter \ref{ch:construction} or, more specifically, by its implementation as described in Chapter \ref{ch:implementation}. We will see whether our assumption that function fields with low genus will lead to sets with lower discrepancies was justified, will compare the sets to pseudo random point sets generated with different programs and will examine whether we can observe a connection between the constant field and the discrepancy of the resulting point sets. After this we will examine the time needed to compute such sets and see whether, in comparison to pseudo-random numbers, it is worth the effort.\\
To obtain a large number of function fields we use Magma's function field database, choose fields that match our particular demand and construct the divisor $D$ always in the same way. For example, to get function fields of degree $3$ over the constant field $\mathbb{F}_5$ (ie. extensions of degree $3$ of the rational function field over $\mathbb{F}_5$) and choose only those with genus $2$ we use the following code.
\begin{lstlisting}[frame=single]
db := FunctionFieldDatabase(5, 3);
// Display the number of function fields
NumberOfFields(db);
// Choose only those with genus 2
dbg2 := sub<db | : Genus := 2>;
// Display the number of these fields
NumberOfFields(dbg2);
// Store the fields in a list
FF := FunctionFields(dbg2);
\end{lstlisting}
If not stated differently all point sets are created by the code below. Here only those fields are considered that, in connection with the divisor constructed there, fulfill the requirements of the algorithm.
\begin{lstlisting}[frame=single]
intrinsic TestFields(functionfields::SeqEnum, number::RngIntElt, \
dimension::RngIntElt, precision::RngIntElt) -> SeqEnum, SeqEnum, SeqEnum
	{Returns LDSs together with corresponding fields and \
	computation time for feasible input fields.}
	points := [];
	fields := [];
	l := [];
	u := [];
	t := [];
	counter := 1;
	for ffield in functionfields do
		P := Places(ffield, 1);
		if (#P eq 0) then
			// Field has no rational places
			continue;
		end if;
		if (not exists{x : x in InfinitePlaces(ffield) | \
		Degree(x) eq 1}) then
			// No infinite rational place
			continue;
		end if;
		// The following if is only needed for our way
		// of constructing the divisor D
		if (Genus(ffield) gt #P) then
			// Not enough places of Degree 1
			continue;
		end if;
		// Initialize D as divisor
		D := P[1] - P[1];
		for j in [1..Genus(ffield)] do
			D := D + P[j];
		end for;
		if (Dimension(RiemannRochSpace(D)) ne 1) then
			// l(D) != 1
			continue;
		end if;
		if (#[ p : p in Places(ffield, 1) | \
		IsFinite(p)] lt dimension) then
			// #FinitePlaces < dimension
			continue;
		end if;
		s := Realtime();
		points[counter] := \
		ConstructLDS(ffield, D, number, dimension, precision);
		e := Realtime();
		t[counter] := e-s;
		fields[counter] := ffield;
		counter +:= 1;
	end for;
	return points, fields, t;
end intrinsic;
\end{lstlisting}
Note that the test whether we have at least $g(K)$ rational places only ensures our way of creating the divisor $D$ as the sum of the first $g(K)$ rational places. If $D$ is constructed otherwise this could be omitted. However, for certain function fields the generation \texttt{Places(K, i)} of the set of all places of degree $i$ can take very long for $i > 1$ so we only relied on rational places.

\section{Different Fields}
\begin{table}
	\centering
	\begin{tabular}{r|r|r|r|r|r|r|r}
		Constant field & Degree & Genus & \#Samples & $l$ & $u$ & SD $l$ & SD $u$\\
		\hline
		$\mathbb{F}_2$ & $2$ & $1$ & $9$ & $6.66$ & $81.95$ & $0.64$ & $1.00$ \\
		$\mathbb{F}_2$ & $2$ & $2$ & $17$ & $8.77$ & $82.61$ & $1.30$ & $1.20$ \\
		$\mathbb{F}_2$ & $2$ & $3$ & $25$ & $14.89$ & $84.84$ & $4.41$ & $1.04$ \\
		\hline
		$\mathbb{F}_4$ & $2$ & $1$ & $15$ & $10.77$ & $85.77$ & $1.51$ & $1.30$ \\
		$\mathbb{F}_4$ & $2$ & $2$ & $54$ & $35.32$ & $103.61$ & $5.34$ & $5.28$ \\
		$\mathbb{F}_4$ & $2$ & $3$ & $46$ & $98.49$ & $146.88$ & $7.45$ & $14.29$ \\
		$\mathbb{F}_4$ & $2$ & $4$ & $100$ & $93.73$ & $136.54$ & $1.45$ & $5.61$ \\
		\hline
		$\mathbb{F}_4$ & $3$ & $0$ & $18$ & $7.60$	& $81.49$ & $0.94$ & $0.75$ \\
		$\mathbb{F}_4$ & $3$ & $1$ & $70$ & $10.28$ & $83.14$ & $1.60$ & $1.18$ \\
		$\mathbb{F}_4$ & $3$ & $2$ & $107$ & $24.07$ & $91.74$ & $10.56$ & $7.93$ \\
		$\mathbb{F}_4$ & $3$ & $3$ & $306$ & $38.37$ & $106.84$ & $22.82$ & $20.78$ \\
		\hline
		$\mathbb{F}_4$ & $5$ & $4$ & $120$ & $44.18$ & $110.98$ & $46.77$ & $37.05$ \\
		\hline
		$\mathbb{F}_5$ & $2$ & $0$ & $30$ & $7.02$ & $81.65$ & $0.70$ & $0.60$ \\
		$\mathbb{F}_5$ & $2$ & $1$ & $101$ & $12.38$ & $84.79$	& $1.84$ & $1.57$ \\
		$\mathbb{F}_5$ & $2$ & $2$ & $107$ & $39.01$ & $97.51$	& $6.43$ & $7.62$ \\
		$\mathbb{F}_5$ & $2$ & $3$ & $114$ & $205.14$ & $242.85$ & $9.03$ & $16.66$ \\
		\hline
		$\mathbb{F}_5$ & $3$ & $2$ & $240$ & $28.98$ & $96.40$ & $17.28$ & $12.89$ \\
		\hline
		$\mathbb{F}_5$ & $4$ & $1$ & $67$ & $11.51$ & $85.62$ & $1.61$ & $1.59$ \\
		$\mathbb{F}_5$ & $4$ & $2$ & $65$ & $37.27$ & $102.30$ & $19.27$ & $13.02$ \\
		$\mathbb{F}_5$ & $4$ & $3$ & $64$ & $225.14$ & $269.31$ & $35.17$ & $32.45$ \\
		\hline
		$\mathbb{F}_7$ & $9$ & $7$ & $40$ & $22.98$ & $92.03$ & $22.56$ & $16.90$
	\end{tabular}
	\caption{The mean of the discrepancy lower bounds $l$ and upper bounds $u$ along with their standard deviation for sets of $10000$ points, generated by a variety of function fields $K/\mathbb{F}_q$ of different degree and genus, using $j_\text{max} = 10$.}
	\label{tab:DiscMean}
\end{table}
In Table \ref{tab:DiscMean} the means of the discrepancy lower and upper bounds for point sets generated by different function fields are listed. We can see that for function fields over the same constant field and with same degree we indeed have lower discrepancy bounds if we have lower genus (apart from the small irregularity for degree $2$ over $\mathbb{F}_4$ at genus $3$ and $4$. However, this is most likely due to different bijections and other factors as we see the standard deviation at genus $3$ is comparatively high).\\
If we expand our look to different degrees we see that function fields of higher degree seem to yield better point sets in view of the discrepancy. Whether this is based on the type of fields in the database or can be proven universally could be the subject of future research.\\
Comparing the discrepancy by different constant fields (in this case $\mathbb{F}_2$, $\mathbb{F}_4$ and $\mathbb{F}_5$) we see that function fields with smaller base fields seem to generate sets of lower discrepancy. Only considering the genus $1$, $2$ and $3$ we get a lower discrepancy bound mean of $10.11$, $48.19$ and $85.51$ for function fields with degree $2$ and constant field $\mathbb{F}_2$, $\mathbb{F}_4$ and $\mathbb{F}_5$ respectively. The same holds for the mean of the upper bound with $83.13$, $112.09$ and $141.72$.

\section{Pseudo-Random Point Sets}
In the section above we examined the influence of different factors on the discrepancy of the resulting point set. Now we want to compare the results to pseudo randomly generated point sets. Therefore, we use different random number generators from different programming languages and see what the discrepancy of the generated sequences is. The result can be seen in Table \ref{tab:pseudorandom}. If we compare this to Table \ref{tab:DiscMean} we see that in most cases even the upper discrepancy bound of our low-discrepancy sets is lower than the lower discrepancy bound of pseudo random sets and also the standard deviation of the bounds of the random point sets is much higher.\\
\begin{table}
	\centering
	\begin{tabular}{l|r|r|r|r|r}
		PRNG & \#Sets & $l$ & $u$ & SD $l$ & SD $u$ \\
		\hline
		\texttt{Random()} in Magma & $265$ & $117.09$ & $176.23$ & $25.20$ & $24.68$ \\
		\texttt{rand()} in C & $100$ & $121.14$ & $178.71$ & $26.32$ & $26.62$ \\
		\texttt{random.uniform} in Python & $60$ & $118.15$ & $175.00$ & $27.27$ & $27.44$ \\
		\texttt{Math.random()} in Java & $60$ & $123.08$ & $181.87$ & $25.53$ & $25.32$
	\end{tabular}
	\caption{The mean discrepancy bounds $l$ and $u$ of sets of $10000$ points generated by pseudo-random generators.}
	\label{tab:pseudorandom}
\end{table}
\begin{figure}
		\centering
		\begin{minipage}[t]{.4\textwidth}
			\centering
			\includegraphics[width=\textwidth]{figures/rand500}
		\end{minipage}%
		\begin{minipage}[t]{.4\textwidth}
			\centering
			\includegraphics[width=\linewidth]{figures/first500}
		\end{minipage}
		\begin{minipage}[t]{.4\textwidth}
			\centering
			\includegraphics[width=\textwidth]{figures/rand1000}
		\end{minipage}%
		\begin{minipage}[t]{.4\textwidth}
			\centering
			\includegraphics[width=\linewidth]{figures/first1000}
		\end{minipage}
		\begin{minipage}[t]{.4\textwidth}
			\centering
			\includegraphics[width=\textwidth]{figures/rand3000}
		\end{minipage}%
		\begin{minipage}[t]{.4\textwidth}
			\centering
			\includegraphics[width=\linewidth]{figures/first3000}
		\end{minipage}
		\begin{minipage}[t]{.4\textwidth}
			\centering
			\includegraphics[width=\textwidth]{figures/rand10000}
		\end{minipage}%
		\begin{minipage}[t]{.4\textwidth}
			\centering
			\includegraphics[width=\linewidth]{figures/first10000}
		\end{minipage}
		\caption{The first $500$, $1000$, $3000$, and $10000$ points of a pseudo-random point sequence (left) and a low-discrepancy sequence (right).}
		\label{fig:firstpoints}
\end{figure}
In Figure \ref{fig:firstpoints} the first points of a low-discrepancy sequence as well as of a pseudo-random point sequence are plotted. You can see that the low-discrepancy sequence is constructed by a pattern where following points will fill holes in the pattern and create a finer version of it. This pattern is not unique and differs from sequence to sequence and may sometimes even be not noticeable.

\section{Effect of the Bijections}
In all examples above we chose the bijections $\psi_r$ and $\eta_j^{(i)}$ randomly as explained in Chapter \ref{ch:implementation}. But what if $\psi_r = \psi_{r'}$ and $\eta_j^{(i)} = \eta_{j'}^{(i')}$ for all $1 \leq i, i' \leq s$, $1 \leq j, j' \leq j_\text{max}$ and $0 \leq r, r' \leq r_\text{max}$? Does this affect the discrepancy of the arising point sets?\\
\begin{table}
	\centering
	\begin{tabular}{r|r|r|r|r|r|r|r|r|r}
		Genus & $l$ & \#Sets & $l_1$ & $l_2$ & $l_3$ & $u$ & $u_1$ & $u_2$ & $u_3$ \\
		\hline
		$0$ & $30$ & $7.02$ & $7.09$ & $6.76$ & $7.32$ & $81.65$ & $81.62$ & $81.64$ & $81.68$ \\
		$1$ & $100$ & $12.38$ & $14.97$ & $12.81$ & $17.40$ & $84.79$ & $86.03$ & $84.22$ & $88.23$ \\
		$2$ & $100$ & $39.01$ & $54.60$ & $44.50$ & $47.26$ & $97.51$ & $105.37$ & $100.84$ & $109.58$ \\
		$3$ & $100$ & $205.14$ & $273.66$ & $236.39$ & $61.70$ & $242.85$ & $310.91$ & $275.50$ & $132.47$
	\end{tabular}
	\caption{Mean lower and upper discrepancy bounds $l$ and $u$ for sets of $10000$ points generated by randomly chosen, different bijections as well as bounds $l_k$, $u_k$ for the same bijections chosen for any $i, j, r$. All point sets were constructed by means of function fields over $\mathbb{F}_5$ of degree $2$.}
	\label{tab:bieffect}
\end{table}
In Table \ref{tab:bieffect} the results of this test can be seen. We modified our implementation such that all bijections that were generated were exactly the same. We did this three times with different predefined mappings and computed the lower and upper discrepancy bound. As you can see, usually this results in slightly worse discrepancy bounds although the real discrepancy could very well be nearly the same. The most outstanding difference occurs for genus $3$, where one of the fixed bijections lead to point sets that are considerably better discrepancy-wise than random bijections or the other fixed ones. One reason could be that different bijections do differently well for certain function fields. By using varying bijections (either predefined or randomly chosen) we, in general, end up with better point sets than with fixed bijections, although we could find ones that are better suited for the specific fields we use.

\section{Computation Time}
Now that we gave evidence that our algorithm yields point sets with lower discrepancies than the usual pseudo-random number generators we want to examine the computation time needed to construct such sets. Before we look at the running time of different examples we take a look at the algorithm (see Chapter \ref{ch:implementation}) itself and analyze it.\\
Let $1\leq s, j_\text{max}, r_\text{max}$ and $n_\text{max}$ be fixed. The first thing we notice is that the number of bijections $\eta_j^{(i)}$ that we need does not depend on the number of points $n_\text{max}$ we want to generate. The same holds for the number of times we have to compute Riemann-Roch spaces and their quotients. If we look at how we construct the bijections we see that one bijection can be generated in $O(q)$. Since we need $r_\text{max} + s  j_\text{max}$ bijections this can be done in
$$O(q(r_\text{max} + s  j_\text{max})).$$
Next we have to find the elements $c_{j, r}^{(i)}$. To do this we have to find $s j_\text{max}$ many elements $k_j^{(i)}$. Therefore we need to compute two Riemann-Roch spaces, a quotient space and an expansion. After this we copy the $r_\text{max}$ values that we need. This results in a complexity of
$$O(s  j_\text{max}  (L(j_\text{max}) + Q(j_\text{max}) + E(r_\text{max}) + r_\text{max})),$$
where $L(j_\text{max})$ is the complexity of computing Riemann-Roch spaces of divisors of degree about $j_\text{max}$, $Q(j_\text{max})$ the complexity of computing quotients of these spaces and $E(r_\text{max})$ the time needed to find an expansion with precision $r_\text{max}$.\\
Finally we can compute the point sets. For each of the $n_\text{max}$ points we need to compute the coefficients of the representation of $n$ in base $q$ and the coordinates for all $s$ dimensions. To do the latter we essentially iterate through all $j$ and sum up $r_\text{max}$ many elements, invoking $r_\text{max} + 1$ bijections. If we assume invoking a bijection has complexity $q$ we get a total complexity of the second part of
$$O(n_\text{max}  (R(n_\text{max}, q) + s  j_\text{max}  (r_\text{max}  (1 + q) + q))).$$
But what does this mean for us? We see that the generation of points that are more precise (ie. with a larger $j_\text{max}$) takes longer, where the initialization of the algorithm (finding bijections and $c_{j, r}^{(i)}$) is affected the most. This, however, is not surprising. But what turns out to be a disadvantage is the dependency of the running time on $n_\text{max}$ if we remember that $r_\text{max}$ depends on $n_\text{max}$ by $r_\text{max} \in O(\ln n_\text{max})$. Whereas for the usual pseudo-random generators the complexity depends linearly on the points needed, in our case we have a dependency similar to $n_\text{max}\ln n_\text{max}$. This results in longer running times for larger numbers of points and could vanish the advantage we gain by the lower discrepancy of the result.\\
In the next section we will examine how the discrepancy can influence numerical computations and whether it is worth it to generate low-discrepancy sequences as explained in the chapters before.\\
\begin{table}
	\centering
	\begin{tabular}{r|r|r|r}
		\#Points & \#Sets & Mean comp. time & Mean comp. time per 100 Points \\
		\hline
		$1$ & $1390$ & $0.45$ & --- \\
		$100$ & $420$ & $0.60$ & $0.16$ \\
		$1000$ & $960$ & $0.69$ & $0.07$ \\
		$10000$ & $960$ & $2.11$ & $0.02$ \\
		$100000$ & $960$ & $18.84$ & $0.02$ \\
		$1000000$ & $122$ & $189.17$ & $0.19$ \\
	\end{tabular}
	\caption{Mean time (in seconds) needed to compute two-dimensional points using function fields over $\mathbb{F}_5$ of degree $2$ and genus $2$.}
	\label{tab:time5-2-2}
\end{table}
\begin{table}
	\centering
	\begin{tabular}{r|r|r|r}
		\#Points & \#Sets & Mean comp. time & Mean comp. time per 100 Points \\
		\hline
		$1$ & $540$ & $0.07$ & --- \\
		$1000$ & $540$ & $0.20$ & $0.0197$ \\
		$10000$ & $540$ & $1.55$ & $0.0155$ \\
		$100000$ & $540$ & $17.51$ & $0.0175$ \\
		$1000000$ & $167$ & $191.67$ & $0.0192$
	\end{tabular}
	\caption{Mean time (in seconds) needed to compute single-dimensional points using function fields over $\mathbb{F}_4$ of degree $2$ and genus $3$.}
	\label{tab:time4-2-3}
\end{table}
We conclude this section with some practical tests. In Table \ref{tab:time5-2-2} and \ref{tab:time4-2-3} the mean computation time for different numbers of points is listed. At first the computation time per $100$ points decreases since here the initialization and computation of the Riemann-Roch spaces is the most expensive part. Later this is exceeded by the actual computation of the points and the time per $100$ points increases. In Table \ref{tab:time4-2-3} we only compute one-dimensional points so this turning point is reached faster than in Table \ref{tab:time5-2-2} since the initialization also depends on the dimension. The effect of the dimension on the computation time can be seen in Table \ref{tab:imodtime} in more detail.
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{figures/nmodtime}
	\captionof{figure}{Mean time (in seconds) needed to generate sets of $100,~200,\ldots,~30000$ points using function fields over $\mathbb{F}_5$ (blue) plotted against a linear dependency (green). The ``jumps'' in the blue line are exactly where $r_\text{max}$ had to be increased (at $n_\text{max} = 5^4,~5^5,~5^6$).}
	\label{fig:nmodtime}
\end{figure}In Figure \ref{fig:nmodtime} the running time for computing sets of different numbers of points is plotted. Here you can see the logarithmic influence at the ``jumps'' in the blue line.\\
\begin{table}
	\centering
	\begin{tabular}{r|r|r}
		$s$ & \#Sets & Mean comp. time \\
		\hline
		$1$ & $540$ & $0.20$ \\
		$2$ & $540$ & $0.39$ \\
		$3$ & $486$ & $0.60$ \\
		$4$ & $324$ & $0.84$ \\
		$5$ & $216$ & $1.00$ \\
		$6$ & $54$ & $1.36$
	\end{tabular}
	\caption{Mean computation time (in seconds) for different sets of $1000$ points with varying dimension. The underlying function field has genus $3$, degree $2$ and constant field $\mathbb{F}_4$. Here $j_\text{max} = 20$ is used.}
	\label{tab:imodtime}
\end{table}
Finally, in Figure \ref{fig:jmodtime} the effect of $j_\text{max}$ on the computation time can be seen. As you can see, it is also quite heavy. However, usually a $j_\text{max}$ of $100$ is much more than needed. A test with the same fields and $j_\text{max} = 10, 20, 30$ gave discrepancy bounds of $29.42 \leq \Delta(1000) \leq 34.18$, $29.91 \leq \Delta(1000) \leq 34.64$ and $29.68 \leq \Delta(1000) \leq 34.42$, respectively, so even here there is no difference measurable.
\begin{figure}
	\centering
	\begin{minipage}[!b]{.4\textwidth}
		\centering
		\begin{tabular}{r|r}
			$j_\text{max}$ & Mean comp. time \\
			\hline
			$10$ & $0.08$ \\
			$20$ & $0.20$ \\
			$30$ & $0.39$ \\
			$40$ & $0.62$ \\
			$50$ & $0.95$ \\
			$60$ & $1.33$ \\
			$70$ & $1.81$ \\
			$80$ & $2.37$ \\
			$90$ & $3.00$ \\
			$100$ & $3.74$ \\
			$110$ & $4.57$ \\
			$120$ & $5.50$ \\
			$130$ & $6.53$ \\
			$140$ & $7.61$ \\
			$150$ & $8.84$ \\
			$160$ & $10.20$
		\end{tabular}
	\end{minipage}
	\begin{minipage}[!b]{.58\textwidth}
		\centering
		\includegraphics[width=\textwidth]{figures/jmodtime}
	\end{minipage}%
	\caption{Mean computation time (in seconds) for $540$ different sets of $1000$ one-dimensional points each with varying $j_\text{max}$. Underlying function fields have genus $3$, degree $2$ and constant field $\mathbb{F}_4$. On the right the mean computation time is plotted against $j_\text{max}/10$.}
	\label{fig:jmodtime}
\end{figure}

\section{Numerical Examples}
We showed that our low-discrepancy sets have significantly better discrepancies than pseudo-random sets. But does this really influence numerical computations measurably? We tested this using the Monte Carlo integration to compute the two-dimensional integral of three different functions. Then we compared the results to the real integral (up to a precision of $10^{-15}$). The magnitude of the errors can be seen in Tables \ref{tab:MC1}, \ref{tab:MC2} and \ref{tab:MC3}. We see that, using low-discrepancy sets instead of pseudo-random sets, the results converge much faster.\\
\begin{table}
	\centering
	\begin{tabular}{r|r|r}
		Number of points & Error for PRS & Error for LDS \\
		$10$ & $10^{-3}$ & $10^{-3}$ \\
		$100$ & $10^{-3}$ & $10^{-3}$ \\
		$1000$ & $10^{-3}$ & $10^{-5}$ \\
		$10000$ & $10^{-4}$ & $10^{-5}$ \\
		$100000$ & $10^{-4}$ & $10^{-6}$
	\end{tabular}
	\captionof{table}{Error of the Monte Carlo integration of $0.3sin(10xy)$ in the unit square using low-discrepancy sets and pseudo-random number sets.\\ \vspace{2em}}
	\label{tab:MC1}
%\end{table}
%\begin{table}
	\centering
	\begin{tabular}{r|r|r}
		Number of points & Error for PRS & Error for LDS \\
		$10$ & $10^{-2}$ & $10^{-3}$ \\
		$100$ & $10^{-2}$ & $10^{-3}$ \\
		$1000$ & $10^{-3}$ & $10^{-4}$ \\
		$10000$ & $10^{-4}$ & $10^{-5}$ \\
		$100000$ & $10^{-4}$ & $10^{-7}$
	\end{tabular}
	\captionof{table}{Error of the Monte Carlo integration of $0.3exp(x)sin(3x)-cos(2y-1)$ in the unit square using low-discrepancy sets and pseudo-random number sets.\\ \vspace{2em}}
	\label{tab:MC2}
%\end{table}
%\begin{table}
	\centering
	\begin{tabular}{r|r|r}
		Number of points & Error for PRS & Error for LDS \\
		$10$ & $10^{-2}$ & $10^{-2}$ \\
		$100$ & $10^{-2}$ & $10^{-4}$ \\
		$1000$ & $10^{-3}$ & $10^{-4}$ \\
		$10000$ & $10^{-4}$ & $10^{-7}$ \\
		$100000$ & $10^{-5}$ & $10^{-7}$
	\end{tabular}
	\captionof{table}{Error of the Monte Carlo integration of $y/cos(x)$ in the unit square using low-discrepancy sets and pseudo-random number sets.}
	\label{tab:MC3}
\end{table}