% -------------------------------------------------------------------
%  @LaTeX-class-file{
%     filename        = "Dissertate.cls",
%     version         = "2.0",
%     date            = "25 March 2014",
%     codetable       = "ISO/ASCII",
%     keywords        = "LaTeX, Dissertate",
%     supported       = "Send email to suchow@post.harvard.edu.",
%     docstring       = "Class for a dissertation."
% --------------------------------------------------------------------

%\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{Dissertate}[2014/03/25 v2.0 Dissertate Class]
\LoadClass[12pt, a4paper]{book}
%\LoadClass[12pt, oneside, a4paper]{book}

% Text layout.
\RequirePackage[width=5.75in, a4paper]{geometry}
\usepackage{ragged2e}
%\RaggedRight
\RequirePackage{graphicx}
%\usepackage{fixltx2e}
\parindent 12pt
\RequirePackage{lettrine}
\RequirePackage{setspace}
%\RequirePackage{verbatim}

% Fonts.
\RequirePackage{color}
\RequirePackage{xcolor}
\usepackage{hyperref}
\RequirePackage{url}
\RequirePackage{amssymb}
\RequirePackage{mathtools}
\RequirePackage{amsmath}
\RequirePackage[english]{babel}
%\RequirePackage{garamondx}
\RequirePackage[garamondx,bigdelims]{newtxmath}
%\useosfI % or \useosfI, must follow math package
%\RequirePackage{mathspec}
\RequirePackage{fontspec}

\begin{filecontents*}{ligatures.fea}
	languagesystem DFLT dflt;
	languagesystem latn dflt;
	# Ligatures
	feature liga {
		sub \f \i by \fi;
		sub \f \l by \fl;
		sub \f \f by \ff;
		sub \f \f \i by \ffi;
		sub \f \f \l by \ffl;
		sub \I \J by \IJ;
		sub \i \j by \ij;
	} liga;
\end{filecontents*}

\setmainfont[Path	= ./fonts/Garamondx/,
	BoldFont        = NewG8-Bol.otf,
	ItalicFont      = NewG8-Ita.otf,
	BoldItalicFont  = NewG8-BolIta.otf,
	SmallCapsFont   = NewG8-Reg-SC.otf,
	FeatureFile 	= ligatures.fea]{NewG8-Reg.otf}

%\RequirePackage{ebgaramond-maths}

%\setmathsfont(Latin,Greek)[Numbers={Proportional}]{EB Garamond}

%\setmathrm{EB Garamond}
%\AtBeginEnvironment{tabular}{\addfontfeature{RawFeature=+tnum}}
\widowpenalty=300
\clubpenalty=300
%\setromanfont[Numbers=OldStyle, Ligatures={Common, TeX}, Scale=1.0]{EB Garamond}
%\newfontfamily{\smallcaps}[RawFeature={+c2sc,+scmp}]{EB Garamond}
%\setsansfont[Scale=MatchLowercase, BoldFont={Lato Bold}]{Lato Regular}
\setmonofont[Scale=MatchLowercase]{Source Code Pro}
%\setmonofont{Courier New}
\RequirePackage[labelfont={bf,sf,footnotesize,singlespacing},
                textfont={sf,footnotesize,singlespacing},
                justification=centering,
                singlelinecheck=false,
                margin=0pt,
                figurewithin=chapter,
                tablewithin=chapter]{caption} % Originally justification={justified,RaggedRight}
\renewcommand{\thefootnote}{\fnsymbol{footnote}}

% Headings and headers.
\RequirePackage{fancyhdr}
\RequirePackage[tiny, md, sc]{titlesec}
\setlength{\headheight}{15pt}
\pagestyle{plain}
\RequirePackage{titling}

% Front matter.
\setcounter{tocdepth}{1}
\usepackage[titles]{tocloft}
\usepackage[titletoc]{appendix}
\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}
\renewcommand{\cftchapfont}{\normalsize \scshape}
\renewcommand\listfigurename{Listing of figures}
\renewcommand\listtablename{Listing of tables}

% Endmatter
\renewcommand{\setthesection}{\arabic{chapter}.A\arabic{section}}

% References.
%\renewcommand\bibname{References}
%\RequirePackage[super,comma,numbers]{natbib}
%\renewcommand{\bibnumfmt}[1]{[#1]}

% Fancy chapters
%Options: Sonny, Lenny, Glenn, Conny, Rejne, Bjarne, Bjornstrup
%\usepackage[Lenny]{fncychap}
\RequirePackage[palatino]{quotchap}
\renewcommand*{\chapterheadstartvskip}{\vspace*{-0.5\baselineskip}}
\renewcommand*{\chapterheadendvskip}{\vspace{1.3\baselineskip}}


% An environment for paragraph-style section.
\providecommand\newthought[1]{%
   \addvspace{1.0\baselineskip plus 0.5ex minus 0.2ex}%
   \noindent\textsc{#1}%
}

% Align reference numbers so that they do not cause an indent.
%%%%\newlength\mybibindent
%%%%\setlength\mybibindent{0pt}
%\renewenvironment{thebibliography}[1]
%    {\chapter*{\bibname}%
%     \@mkboth{\MakeUppercase\bibname}{\MakeUppercase\bibname}%
%     \list{\@biblabel{\@arabic\c@enumiv}}
%          {\settowidth\labelwidth{\@biblabel{999}}
%           \leftmargin\labelwidth
%            \advance\leftmargin\dimexpr\labelsep+\mybibindent\relax\itemindent-\mybibindent
%           \@openbib@code
%           \usecounter{enumiv}
%           \let\p@enumiv\@empty
%           \renewcommand\theenumiv{\@arabic\c@enumiv}}
%     \sloppy
%     \clubpenalty4000
%     \@clubpenalty \clubpenalty
%     \widowpenalty4000%
%     \sfcode`\.\@m}
%    {\def\@noitemerr
%      {\@latex@warning{Empty `thebibliography' environment}}
%     \endlist}

% Some definitions.
\def\advisor#1{\gdef\@advisor{#1}}
\def\coadvisorOne#1{\gdef\@coadvisorOne{#1}}
\def\coadvisorTwo#1{\gdef\@coadvisorTwo{#1}}
\def\committeeInternal#1{\gdef\@committeeInternal{#1}}
\def\committeeInternalOne#1{\gdef\@committeeInternalOne{#1}}
\def\committeeInternalTwo#1{\gdef\@committeeInternalTwo{#1}}
\def\committeeExternal#1{\gdef\@committeeExternal{#1}}
\def\degreeyear#1{\gdef\@degreeyear{#1}}
\def\degreemonth#1{\gdef\@degreemonth{#1}}
\def\degreeterm#1{\gdef\@degreeterm{#1}}
\def\degree#1{\gdef\@degree{#1}}
\def\department#1{\gdef\@department{#1}}
\def\field#1{\gdef\@field{#1}}
\def\university#1{\gdef\@university{#1}}
\def\universitycity#1{\gdef\@universitycity{#1}}
\def\universitystate#1{\gdef\@universitystate{#1}}
\def\programname#1{\gdef\@programname{#1}}
\def\pdOneName#1{\gdef\@pdOneName{#1}}
\def\pdOneSchool#1{\gdef\@pdOneSchool{#1}}
\def\pdOneYear#1{\gdef\@pdOneYear{#1}}
\def\pdTwoName#1{\gdef\@pdTwoName{#1}}
\def\pdTwoSchool#1{\gdef\@pdTwoSchool{#1}}
\def\pdTwoYear#1{\gdef\@pdTwoYear{#1}}
% School name and location
\university{TU Kaiserslautern}
\universitycity{Kaiserslautern}
\universitystate{Rheinland-Pfalz}

% School color found from university's graphic identity site:
% http://www.princeton.edu/communications/services/image/graphic/color
\definecolor{SchoolColor}{cmyk}{0.9, 0.67, 0, 0.42}
\definecolor{chaptergrey}{cmyk}{0.9, 0.67, 0, 0.42}
\definecolor{mygrey}{RGB}{136, 136, 136}

\hypersetup{
    colorlinks,
    citecolor=SchoolColor,
    filecolor=black,
    linkcolor=black,
    urlcolor=SchoolColor,
}

% Formatting guidelines found in:
% http://www.princeton.edu/~mudd/thesis/MuddDissertationRequirements.pdf
\renewcommand{\frontmatter}{
	\input{frontmatter/personalize}
	\maketitle
	% \copyrightpage
	% \abstractpage
	\tableofcontents
	% \listoffigures % optional
	% \dedicationpage
	% \acknowledgments
}

\renewcommand{\maketitle}{
	\thispagestyle{empty}
	\vspace*{\fill}
	\begin{center}
		\includegraphics[width=15em]{figures/logo} \\
		\sc \large Department of Mathematics
	\end{center}
	\vspace{75pt}
	\begin{center}
	\Huge \textcolor{SchoolColor}{\thetitle} \normalsize \\
	\vspace{100pt}
	\textsc{\theauthor}\\
	\vspace{50pt}
	\sc
		a thesis\\
	  presented to the faculty\\
	  of TU Kaiserslautern\\
	  in candidacy for the degree\\
    of \@degree\\

%	  \vspace{35pt}
%	  recommended for acceptance\\
%	  by the Department of\\
%		\@department\\
		\vspace{25pt}
	  Adviser: \@advisor \\
		\vspace{15pt}
	  \@degreemonth\ \@degreeyear
	\rm
	\end{center}
	\vspace*{\fill}
}

\newcommand{\copyrightpage}{
	\newpage
	\thispagestyle{empty}
	\vspace*{25pt}
	\begin{center}
	\scshape \noindent \small \copyright \  \small Copyright by \theauthor, \@degreeyear. All rights reserved.
	\end{center}
	\newpage
	\rm
}

\newcommand{\abstractpage}{
	\phantomsection
	\addcontentsline{toc}{chapter}{Abstract}
	\newpage
	\pagenumbering{roman}
	\setcounter{page}{3}
	\pagestyle{fancy}
	\renewcommand{\headrulewidth}{0.0pt}
	\vspace*{35pt}
	\begin{center}
	\scshape Abstract \\ \rm
	\end{center}
	\input{frontmatter/abstract}
	\vspace*{\fill}
	\newpage \lhead{} \rhead{}
	\cfoot{\thepage}
}

\newcommand{\dedicationpage}{
	\newpage \thispagestyle{fancy} \vspace*{\fill}
	\scshape \noindent \input{frontmatter/dedication}
	\vspace*{\fill} \newpage \rm
}

\newcommand{\acknowledgments}{
	\chapter*{Acknowledgments}
	\noindent
	\input{frontmatter/thanks}
	\vspace*{\fill} \newpage
	\setcounter{page}{1}
	\pagenumbering{arabic}
}

\renewcommand{\backmatter}{
   % \begin{appendices}
   %     \include{chapters/appendixA}
   % \end{appendices}
    \input{endmatter/personalize}
    \clearpage
    \bibliographystyle{amsalpha}
	\nocite{*}
    \bibliography{endmatter/bibliography}
    %\printindex
    \addcontentsline{toc}{chapter}{References}
    \begin{appendices}
   %     \include{chapters/appendixA}
	   \include{endmatter/statutorydeclaration}
    \end{appendices}
    
   % \include{endmatter/colophon}
}

% My stuff
\hypersetup{citecolor=black}
\let\openbox\relax
\RequirePackage{amsthm}
\RequirePackage{xpatch}
%\RequirePackage{garamondx}
\RequirePackage[]{algorithmicx}
\RequirePackage{algorithm}
\RequirePackage[]{algpseudocode}
\RequirePackage{listings}
\RequirePackage{makeidx}
\RequirePackage{diagbox}
\makeindex

\newcommand{\sslash}{\mathbin{/\mkern-6mu/}}
% To indent theoremstyles
%\usepackage{etoolbox}
%\makeatletter
%\patchcmd{\@thm}{\trivlist}{\list{}{\leftmargin=2.5em}}{}{}
%\patchcmd{\@endtheorem}{\endtrivlist}{\endlist}{}{}
%\makeatother
%\DeclareTextCommand{\nobreakspace}{T1}{\leavevmode\nobreak\ }

\newcommand*\Let[2]{\State #1 $\gets$ #2}
\algrenewcommand\alglinenumber[1]{
	{\sf\footnotesize\addfontfeatures{Colour=888888,Numbers=Monospaced}#1}}
\algrenewcommand\algorithmicrequire{\textbf{Precondition:}}
\algrenewcommand\algorithmicensure{\textbf{Postcondition:}}
\algrenewcommand\algorithmicrequire{\textsc{Input:}}
\algrenewcommand\algorithmicensure{\textsc{Output:}}
%\algrenewcommand\algorithmiccomment{$\Rightarrow$ }
\algrenewcommand\Return{\State \algorithmicreturn{} }

\algnewcommand\algorithmicinput{\textsc{Input:}}
\algnewcommand\INPUT{\item[\algorithmicinput]}
\algnewcommand\algorithmicoutput{\textsc{Output:}}
\algnewcommand\OUTPUT{\item[\algorithmicoutput]}

\algnewcommand\INTRINSIC[3]{\State \textbf{intrinsic} #1(#2) -> #3}
\algnewcommand\ENDINTRINSIC{\textbf{end intrinsic;}}


\xpatchcmd{\proof}{\hskip\labelsep}{\hskip3\labelsep}{}{}

\newtheoremstyle{break-italic}% name
{1ex}%      Space above, empty = `usual value'
{1.5ex}%      Space below
{\itshape}% Body font
{\parindent }%         Indent amount (empty = no indent, \parindent = para indent)
{\scshape}% Thm head font
{.}%        Punctuation after thm head
{2ex }% Space after thm head: \newline = linebreak
{}%         Thm head 
\newtheoremstyle{break-roman}% name
{1ex}%      Space above, empty = `usual value'
{1.5ex}%      Space below
{\normalfont}% Body font
{\parindent }%         Indent amount (empty = no indent, \parindent = para indent)
{\scshape}% Thm head font
{.}%        Punctuation after thm head
{2ex }% Space after thm head: \newline = linebreak
{}%         Thm head spec

\theoremstyle{break-italic} 
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\theoremstyle{break-roman}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
%\newtheorem{algorithmus}[theorem]{Algorithm}
\newtheorem{motivation}[theorem]{Motivation}

\DeclareMathOperator{\Quot}{Quot}
\DeclareMathOperator{\ord}{ord}
\DeclareMathOperator{\proj}{proj}
\titleformat{\section}{\normalfont\scshape\large}{\thesection}{1em}{}{}

\lst@definelanguage{Magma}%
{%
	otherkeywords={:=,+:=,-:=,*:=},%
	% functions
	procnamekeys={function,func,intrinsic,procedure,proc},%
	% Booleans
	morekeywords={true,false},%
	% relations
	morekeywords=[2]{adj,and,cat,cmpeq,cmpne,diff,div,eq,ge,gt,in,is,join,le,lt,%
		meet,mod,ne,notadj,notin,notsubset,or,sdiff,subset,xor},%
	% keywords
	morekeywords=[3]{assigned,break,by,case,catch,continue,declare,default,%
		delete,do,elif,else,end,eval,exists,exit,for,forall,fprintf,if,local,%
		not,print,printf,quit,random,read,readi,repeat,restore,save,select,%
		then,time,to,try,until,vprint,vprintf,vtime,when,where,while},%
	% directives
	morekeywords=[4]{clear,forward,freeze,iload,import,load},%
	% error checks
	morekeywords=[5]{assert,assert2,assert3,error,require,requirege,requirerange},%
	% constructors
	morekeywords=[6]{car,comp,cop,elt,ext,frac,hom,ideal,iso,lideal,loc,map,%
		ncl,pmap,quo,rec,recformat,rep,rideal,sub},%
	% other constructors (semi-reserved)
	morekeywords=[7]{AbelianGroup,AdditiveCode,AffineAlgebra,Algebra,%
		AssociativeAlgebra,Character,CliffordAlgebra,Design,Digraph,%
		ExtensionField,FPAlgebra,FiniteAffinePlane,FiniteProjectivePlane,%
		Graph,Group,GroupAlgebra,IncidenceStructure,LieAlgebra,LinearCode,%
		LinearSpace,MatrixAlgebra,MatrixGroup,MatrixRing,Monoid,%
		MultiDigraph,MultiGraph,NearLinearSpace,Network,PartialMap,%
		PermutationGroup,PolycyclicGroup,QuaternionAlgebra,Semigroup,%
		ZModule},%
	% functions
	morekeywords={[8]function,func,intrinsic,procedure,proc,return},%
	sensitive,%
	morecomment=[l]//,%
	morecomment=[s]{/*}{*/},%
	morecomment=[s]{\{}{\}},%
	morestring=[b]"%
}[keywords,procnames,comments,strings]%
\endinput
% End of my stuff